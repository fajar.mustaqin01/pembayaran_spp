<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <meta name="description" content="POS - Bootstrap Admin Template">
    <meta name="keywords" content="admin, estimates, bootstrap, business, corporate, creative, invoice, html5, responsive, Projects">
    <meta name="author" content="Dreamguys - Bootstrap Admin Template">
    <meta name="robots" content="noindex, nofollow">
    <title>Login - Pos admin template</title>

    <link rel="shortcut icon" type="image/x-icon" href="{{asset('assets/img/favicon.jpg')?>">

    <link rel="stylesheet" href="<?= base_url('assets/assets/css/bootstrap.min.css') ?>">

    <link rel="stylesheet" href="<?= base_url('assets/assets/plugins/fontawesome/css/fontawesome.min.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/assets/plugins/fontawesome/css/all.min.css')?>">

    <link rel="stylesheet" href="<?= base_url('assets/assets/parsleyjs/parsley.css')?>">

    <link rel="stylesheet" href="<?= base_url('assets/assets/css/style.css')?>">
</head>
<body class="account-page">

    <div class="main-wrapper">

        <div class="account-content">
            <div class="login-wrapper">
                <div class="login-content">
                    <div class="login-userset">
                        <div class="login-logo">
                            <img src="<?= base_url('assets/assets/img/logo_tk.jpeg')?>" style="width: 100px; height: 100px;" alt="img">
                        </div>
                        <div class="login-userheading">
                            <h3>Log In</h3>
                        </div>
                        <form action="<?= base_url('login') ?>" method="POST" data-parsley-validate>
                            <div class="form-login">
                                <label>Username</label>
                                <div class="form-addons">
                                    <input type="text" name="username" data-parsley-trigger="change" placeholder="Masukaan username anda" required>
                                    <img src="<?= base_url('assets/assets/img/icons/mail.svg')?>" alt="img">
                                </div>
                            </div>
                            <div class="form-login">
                                <label>Password</label>
                                <div class="pass-group">
                                    <input type="password" name="password" class="pass-input" placeholder="Masukan password anda" required>
                                    <span class="fas toggle-password fa-eye-slash"></span>
                                </div>
                            </div>
                            <div class="form-login">
                                <button class="btn btn-login" type="submit">Login</button>                         
                            </div>
                        </form>
                    </div>
                </div>
                <div class="login-img">
                    <img src="<?= base_url('assets/assets/img/login.jpg')?>" alt="img">
                </div>
            </div>
        </div>




    </div>
    <script src="<?= base_url('assets/assets/js/jquery-3.6.0.min.js')?>"></script>

    <script src="<?= base_url('assets/assets/js/feather.min.js')?>"></script>

    <script src="<?= base_url('assets/assets/js/bootstrap.bundle.min.js')?>"></script>

    <script src="<?= base_url('assets/assets/js/script.js')?>"></script>
    <script src="<?= base_url('assets/assets/parsleyjs/parsley.min.js')?>"></script>
    <script src="<?= base_url('assets/assets/parsleyjs/i18n/id.js')?>"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>


    <!-- SweetAlert2 -->
    <?php if ($this->session->flashdata('message')): ?>
        <script>
            Swal.fire({
                icon: '<?= $this->session->flashdata('message')['type']; ?>', // success, error, etc.
                title: '<?= $this->session->flashdata('message')['title']; ?>',
                text: '<?= $this->session->flashdata('message')['text']; ?>',
            });
        </script>
    <?php endif; ?>
</body>
</html>