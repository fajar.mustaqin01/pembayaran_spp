<script src="<?= base_url('assets/assets/js/jquery-3.6.0.min.js')?>"></script>

<script src="<?= base_url('assets/assets/js/feather.min.js')?>"></script>

<script src="<?= base_url('assets/assets/js/jquery.slimscroll.min.js')?>"></script>

<script src="<?= base_url('assets/assets/js/jquery.dataTables.min.js')?>"></script>
<script src="<?= base_url('assets/assets/js/dataTables.bootstrap4.min.js')?>"></script>

<script src="<?= base_url('assets/assets/js/bootstrap.bundle.min.js')?>"></script>

<script src="<?= base_url('assets/assets/plugins/alertify/alertify.min.js') ?>"></script>
<!-- <script src="<?= base_url('assets/assets/plugins/alertify/custom-alertify.min.js') ?>"></script> -->

<script src="<?= base_url('assets/assets/js/moment.min.js')?>"></script>

<script src="<?= base_url('assets/assets/js/bootstrap-datetimepicker.min.js')?>"></script>

<script src="<?= base_url('assets/assets/plugins/apexchart/apexcharts.min.js')?>"></script>
<script src="<?= base_url('assets/assets/plugins/apexchart/chart-data.js')?>"></script>

<script src="<?= base_url('assets/assets/js/script.js')?>"></script>

<script src="<?= base_url('assets/template/admin/')?>assets/libs/select2/dist/js/select2.full.min.js"></script>
<script src="<?= base_url('assets/template/admin/')?>assets/libs/select2/dist/js/select2.min.js"></script>

<script src="<?= base_url('assets/assets/plugins/sweetalert/sweetalert2.all.min.js')?>"></script>
<script src="<?= base_url('assets/assets/plugins/sweetalert/sweetalerts.min.js')?>"></script>

<script>
    $(".select2").select2();
    const site_url = '<?php echo site_url() ?>';



    $(document).ready(function() {
        function formatRupiah(angka, prefix) {
            var number_string = angka.replace(/[^,\d]/g, '').toString(),
                split = number_string.split(','),
                sisa = split[0].length % 3,
                rupiah = split[0].substr(0, sisa),
                ribuan = split[0].substr(sisa).match(/\d{3}/gi);

            if (ribuan) {
                separator = sisa ? '.' : '';
                rupiah += separator + ribuan.join('.');
            }

            rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
            return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
        }

        $('.rupiah').on('keyup', function() {
            $(this).val(formatRupiah($(this).val(), 'Rp. '));
        });
    });

    // function formatTanggal(tanggal) {
    //     // Memisahkan tanggal menjadi komponen hari, bulan, dan tahun
    //     var parts = tanggal.split('-');
    //     var hari = parts[0];
    //     var bulan = parts[1];
    //     var tahun = parts[2];

    //     // Membuat format "YYYY-MM-DD"
    //     var tanggalFormatted = tahun + '-' + bulan + '-' + hari;

    //     return tanggalFormatted;
    // }

</script>

<?php echo '<script>';
if (isset($scripts) && is_array($scripts)) {
    foreach ($scripts as $script) {
        $this->load->view($script);
    }
}
echo '</script>';
?>


<script>

$(document).ready(function(){

    modal_edit_kelas = $('#modal-edit-kelas');


    $('#zero_config').on('click','.ubah-data-kelas', function(){
        id = $(this).data('id');

        $.ajax({
            url : '<?= base_url('get/ubahkelas');?>',
            type : 'post',
            dataType: 'json',
            data : {id: id},
            success: function(data) {
                $('#form_ubah_data_kelas [name="id_ed_k"]').val(data[0].id_);
                $('#form_ubah_data_kelas [name="kode_kelas_ed"]').val(data[0].kode_kelas);
                $('#form_ubah_data_kelas [name="nama_kelas_ed"]').val(data[0].nama_kelas);
                $('#form_ubah_data_kelas [name="level_ed"]').val(data[0].level);

                modal_edit_kelas.modal('show').on('shown.bs,modal');
            },
            error: function(data) {
                alert('error');
            }
        });
    });

});

</script>



