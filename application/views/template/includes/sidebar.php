<div class="sidebar" id="sidebar">
    <div class="sidebar-inner slimscroll">
        <div id="sidebar-menu" class="sidebar-menu">
            <ul>
                <li class="<?php if ($menu == 'beranda') echo 'active'; ?>">
                    <a href="<?= base_url('admin/index') ?>"><i data-feather="home"></i><span> Beranda</span> </a>
                </li>

                <?php if($this->session->userdata('level') == '1') :?>
                
                <li class="submenu">
                    <a href="javascript:void(0);"><i data-feather="user"></i><span> Master Data</span> <span class="menu-arrow"></span></a>
                    <ul>
                        <!-- <li><a href="<?= base_url('admin/instansi') ?>">Instansi</a></li> -->
                        <li><a href="<?= base_url('admin/siswa') ?>"  class="<?php if ($menu == 'siswa') echo 'active'; ?>">Siswa</a></li>
                        <li><a href="<?= base_url('admin/kelas') ?>" class="<?php if ($menu == 'kelas') echo 'active'; ?>">Kelas</a></li>
                        <!-- <li><a href="<?php// echo base_url('admin/biaya') ?>" class="<?php// if ($menu == 'biaya') echo 'active'; ?>">Biaya</a></li> -->
                    </ul>
                </li>

                <?php endif; ?>
                <!-- <li class="submenu">
                    <a href="javascript:void(0);"><i data-feather="tag"></i><span> Pembayaran</span><span class="menu-arrow"></span> </a>
                    <ul>
                        <li><a href="<?= base_url('admin/spp') ?>" class="<?php if ($menu == 'spp') echo 'active'; ?>">SPP</a></li>
                        <li><a href="<?= base_url('admin/non_spp') ?>" class="<?php if ($menu == 'nonspp') echo 'active'; ?>">Non SPP</a></li>
                    </ul>
                </li> -->
                <li>
                    <a href="<?= base_url('admin/spp')?>" class="<?php if ($menu == 'spp') echo 'active'; ?>"><i data-feather="tag"></i><span>Pembayaran SPP</span> </a>
                </li>
                <li>
                    <a href="<?= base_url('admin/pengingat')?>" class="<?php if ($menu == 'pengingat') echo 'active'; ?>"><i data-feather="bell"></i><span>Pengingat Pembayaran</span> </a>
                </li>
                <li>
                    <a href="<?= base_url('admin/laporan_spp')?>" class="<?php if ($menu == 'laporan') echo 'active'; ?>"><i data-feather="file"></i><span>Laporan SPP</span> </a>
                </li>

                <!-- <li>
                    <a href="{{ url('kelas')}}"><i data-feather="users"></i><span> Kelas</span> </a>
                </li>

                <li>
                    <a href="{{ url('spp')}}"><i data-feather="package"></i><span> SPP</span> </a>
                </li> -->

                <?php if($this->session->userdata('level') == '1') :?>

                <li>
                    <a href="<?= base_url('admin/users')?>" class="<?php if ($menu == 'users') echo 'active'; ?>"><i data-feather="settings"></i><span> Pengguna Sistem</span> </a>
                </li>

                <?php endif; ?>

    
            </ul>
        </div>
    </div>
</div>