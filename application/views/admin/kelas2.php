<div class="page-wrapper">
    
    <div class="content container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col">
                    <h3 class="page-title">Data Kelas</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url('admin')?>">Beranda</a></li>
                        <li class="breadcrumb-item active">Data Kelas</li>
                    </ul>
                </div>
            </div>
            <div class="page-btn">
                <a type="button" data-bs-toggle="modal" data-bs-target="#addkelas" class="btn btn-primary"><img src="<?= base_url('assets/assets/img/icons/plus.svg') ?>" alt="img"> Tambah Kelas</a>
                <!-- <a href="{{ url('kelas/create')}}" class="btn btn-added" data-bs-toggle="modal" data-bs-target="#addkelas"><img src="assets/img/icons/plus.svg" alt="img">Tambah Kelas</a> -->
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Data Kelas TK</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-top">
                            <div class="search-set">
                                <div class="search-input">
                                    <a class="btn btn-searchset"><img src="<?= base_url('assets/assets/img/icons/search-white.svg') ?>" alt="img"></a>
                                </div>
                            </div>
                            <div class="wordset">
                                <!-- <ul>
                                    <li>
                                        <a data-bs-toggle="tooltip" data-bs-placement="top" title="pdf"><img src="assets/img/icons/pdf.svg" alt="img"></a>
                                    </li>
                                    <li>
                                        <a data-bs-toggle="tooltip" data-bs-placement="top" title="excel"><img src="assets/img/icons/excel.svg" alt="img"></a>
                                    </li>
                                    <li>
                                        <a data-bs-toggle="tooltip" data-bs-placement="top" title="print"><img src="assets/img/icons/printer.svg" alt="img"></a>
                                    </li>
                                </ul> -->
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table datanew" id="zero_config">
                                <thead class="bg-dark">
                                    <tr>
                                        <th class="text-white">No</th>
                                        <th class="text-white">Kode Kelas</th>
                                        <th class="text-white">Nama Kelas</th>
                                        <th class="text-white">Level</th>
                                        <th class="text-white">Jumlah Siswa</th>
                                        <th class="text-white">Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $nom = 0; $no = 1; foreach ($kelas as $kls): ?>
                                        <tr>
                                            <td><?php echo $no++; ?></td>
                                            <td><?php echo $kls->kode_kelas ?></td>
                                            <td><?php echo $kls->nama_kelas ?></td>
                                            <td><?php echo $kls->level ?></td>
                                            <td><?php echo $jumlah[$nom++] ?> Orang</td>
                                            <td>
                                                <a class="me-3 ubah-data-kelas" type="button" data-id="<?php echo $kls->id_ ?>">
                                                    <img src="<?= base_url('assets/assets/img/icons/edit.svg') ?>" alt="img">
                                                </a>
                                                <a href="<?php echo base_url('hapus/kelas/').$kls->id_ ?>" onclick="return confirm('Anda akan menghapus data kelas <?php echo $kls->nama_kelas ?>')">
                                                    <img src="<?= base_url('assets/assets/img/icons/delete.svg') ?>" alt="img">
                                                </a>
                                            </td>
                                            <!-- <td>
                                                <a href="<?php echo base_url('hapus/kelas/').$kls->id_ ?>" onclick="return confirm('Anda akan menghapus data kelas <?php echo $kls->nama_kelas ?>')" class="badge badge-pill badge-danger"><span class="mdi mdi-delete"></span> Hapus</a> | <button type="button" class="badge badge-pill badge-warning ubah-data-kelas" data-id="<?php echo $kls->id_ ?>"><span class="mdi mdi-credit-card"></span> Ubah</button>
                                            </td> -->
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
    </div>
</div>

        <!-- Modal -->
<div class="modal fade" id="addkelas" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Data Kelas</h5>
                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php echo form_open('tambah/kelas'); ?>
            <div class="modal-body">
                <div class="form-group row">
                    <label for="kode_kelas" class="col-sm-3 text-right control-label col-form-label">Kode Kelas</label>
                    <div class="col-sm-9">
                        <input type="text" name="kode_kelas" class="form-control" id="kode_kelas" placeholder="Masukkan Kode Kelas Disini" required="">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="nama_kelas" class="col-sm-3 text-right control-label col-form-label">Nama Kelas</label>
                    <div class="col-sm-9">
                        <input type="text" name="nama_kelas" class="form-control" id="nama_kelas" placeholder="Masukkan Nama Kelas Disini" required="">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="level" class="col-sm-3 text-right control-label col-form-label">Level Kelas</label>
                    <div class="col-sm-9">
                        <input type="text" name="level" class="form-control" id="level" placeholder="Masukkan Level Kelas Disini" required="">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-9">
                        <button type="submit" class="btn btn-primary"> Simpan</button>
                    </div>
                </div>

            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="modal-edit-kelas" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Ubah Data Kelas</h5>
                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?php echo base_url('ubah/datakelas') ?>" method="post" id="form_ubah_data_kelas">
            <div class="modal-body">
                <div class="form-group row">
                    <label for="kode_kelas" class="col-sm-3 text-right control-label col-form-label">Kode Kelas</label>
                    <div class="col-sm-9">
                        <input type="hidden" name="id_ed_k" value="">
                        <input type="text" name="kode_kelas_ed" class="form-control" id="kode_kelas" placeholder="Masukkan Kode Kelas Disini" required="">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="nama_kelas" class="col-sm-3 text-right control-label col-form-label">Nama Kelas</label>
                    <div class="col-sm-9">
                        <input type="text" name="nama_kelas_ed" class="form-control" id="nama_kelas" placeholder="Masukkan Nama Kelas Disini" required="">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="level" class="col-sm-3 text-right control-label col-form-label">Level Kelas</label>
                    <div class="col-sm-9">
                        <input type="text" name="level_ed" class="form-control" id="level" placeholder="Masukkan Level Kelas Disini" required="">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-9">
                        <button type="submit" class="btn btn-primary"> Simpan</button>
                    </div>
                </div>

            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>

<!-- <div class="modal fade" id="addkelas" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Data Kelas</h5>
                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{route('kelas.store')}}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label>Nama Kelas</label>
                                <input type="text" name="nama_kelas" id="nama_kelas">
                            </div>
                        </div>
        
                        <div class="col-12">
                            <div class="form-group">
                                <label>Tingkat</label>
                                <input type="text" name="tingkat" id="tingkat">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer ">
                        <button type="submit" class="btn btn-submit">Confirm</button>
                        <button type="button" class="btn btn-cancel" data-bs-dismiss="modal">Cancel</button>
                    </div>
                </form>
        </div>
    </div>
</div>
    
@endsection -->