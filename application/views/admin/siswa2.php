<div class="page-wrapper">
    
    <div class="content container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col">
                    <h3 class="page-title">Siswa</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url('admin')?>">Beranda</a></li>
                        <li class="breadcrumb-item active">Siswa</li>
                    </ul>
                </div>
            </div>
            <div class="page-btn">
                <a type="button" data-bs-toggle="modal" data-bs-target="#addsiswa" class="btn btn-primary"><img src="<?= base_url('assets/assets/img/icons/plus.svg') ?>" alt="img"> Tambah Siswa</a>
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Data Kelas TK</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-top">
                            <div class="search-set">
                                <div class="search-input">
                                    <a class="btn btn-searchset"><img src="<?= base_url('assets/assets/img/icons/search-white.svg') ?>" alt="img"></a>
                                </div>
                            </div>
                            <div class="wordset">
                                <!-- <ul>
                                    <li>
                                        <a data-bs-toggle="tooltip" data-bs-placement="top" title="pdf"><img src="assets/img/icons/pdf.svg" alt="img"></a>
                                    </li>
                                    <li>
                                        <a data-bs-toggle="tooltip" data-bs-placement="top" title="excel"><img src="assets/img/icons/excel.svg" alt="img"></a>
                                    </li>
                                    <li>
                                        <a data-bs-toggle="tooltip" data-bs-placement="top" title="print"><img src="assets/img/icons/printer.svg" alt="img"></a>
                                    </li>
                                </ul> -->
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table datanew" id="zero_config">
                                <thead class="bg-dark">
                                    <tr>
                                        <th><input type="checkbox" onchange="checkAll(this)"></th>
                                        <th>No</th>
                                        <th>Nama Siswa</th>
                                        <th>NISN</th>
                                        <th>JK</th>
                                        <th>TTL</th>
                                        <th>Nama Kelas</th>
                                        <th>Kategori</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no = 1; foreach ($siswa as $siswa): ?>
                                        <tr>
                                            <td>
                                                <input type="checkbox" name="check[]" value="<?= $siswa->nisn; ?>">
                                            </td>
                                            <td><?php echo $no++; ?></td>
                                            <td><?php echo $siswa->nama ?></td>
                                            <td><?php echo $siswa->nisn ?></td>
                                            <td>
                                                <?php 
                                                if ($siswa->jk == '1') {
                                                    echo 'Laki-laki';
                                                } elseif ($siswa->jk == '2') {
                                                    echo 'Perempuan';
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <?php echo $siswa->tempat_lahir.', '.tgl_indo($siswa->tg_lahir); ?>
                                            </td>
                                            <td><?php echo $siswa->nama_kelas ?></td>
                                            <td><?php echo $siswa->kategori ?></td>
                                            <td>
                                                <a href="<?php echo base_url('hapus/siswa/').$siswa->ids ?>" onclick="return confirm('Anda akan menghapus data kelas <?php echo $siswa->nama ?>')" class="badge badge-pill badge-danger"><span class="mdi mdi-delete"></span> Hapus</a> | <button type="button" class="badge badge-pill badge-warning ubah-data-siswa" data-id="<?php echo $siswa->ids ?>"><span class="mdi mdi-credit-card"></span> Ubah</button>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
    </div>
</div>

        <!-- Modal -->
<div class="modal fade" id="addsiswa" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Data Kelas</h5>
                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php echo form_open('tambah/siswa'); ?>
                    <div class="modal-body">
                        <div class="form-group row">
                            <label for="nama" class="col-sm-3 text-right control-label col-form-label">Nama Siswa</label>
                            <div class="col-sm-9">
                                <input type="text" name="nama" class="form-control" id="nama" placeholder="Masukkan Nama Siswa Disini" required="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="nisn" class="col-sm-3 text-right control-label col-form-label">Nisn</label>
                            <div class="col-sm-9">
                                <input type="number" name="nisn" class="form-control" id="nisn" placeholder="Masukkan NISN Disini" required="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="jk" class="col-sm-3 text-right control-label col-form-label">JK</label>
                            <div class="col-sm-9">
                                <select name="jk" id="jk" class="form-control" required="">
                                    <option value="">-- Pilih Jenis Kelamin --</option>
                                    <option value="1">Laki-laki</option>
                                    <option value="2">Perempuan</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="tempat_lahir" class="col-sm-3 text-right control-label col-form-label">Tmp Lahir</label>
                            <div class="col-sm-9">
                                <input type="text" name="tempat_lahir" class="form-control" id="tempat_lahir" placeholder="Masukkan Tempat Lahir Disini" required="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="tg_lahir" class="col-sm-3 text-right control-label col-form-label">Tg Lahir</label>
                            <div class="col-sm-9">
                                <input type="date" name="tg_lahir" class="form-control" placeholder="Pilih Tanggal" required="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="kode_kelas" class="col-sm-3 text-right control-label col-form-label">Kelas</label>
                            <div class="col-sm-9">
                                <select name="kode_kelas" id="kode_kelas" class="select2 form-control" style="width: 100%; height:35px;">
                                    <option value="">-- Pilih kelas --</option>
                                    <?php foreach ($kelas as $k): ?>
                                        <option value="<?php echo $k->kode_kelas ?>"><?php echo $k->nama_kelas ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="kategori" class="col-sm-3 text-right control-label col-form-label">Kategori</label>
                            <div class="col-sm-9">
                                <select name="kategori" id="kategori" class="form-control">
                                    <option value="">-- Pilih Kategori Ekonomi --</option>
                                    <option value="1">Atas</option>
                                    <option value="2">Menengah</option>
                                    <option value="3">Bawah</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-9">
                                <button type="submit" class="btn btn-primary"> Simpan</button>
                            </div>
                        </div>

                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>


<!-- Modal -->
<div class="modal fade" id="modal-edit-kelas" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Ubah Data Kelas</h5>
                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?php echo base_url('ubah/datakelas') ?>" method="post" id="form_ubah_data_kelas">
            <div class="modal-body">
                <div class="form-group row">
                    <label for="kode_kelas" class="col-sm-3 text-right control-label col-form-label">Kode Kelas</label>
                    <div class="col-sm-9">
                        <input type="hidden" name="id_ed_k" value="">
                        <input type="text" name="kode_kelas_ed" class="form-control" id="kode_kelas" placeholder="Masukkan Kode Kelas Disini" required="">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="nama_kelas" class="col-sm-3 text-right control-label col-form-label">Nama Kelas</label>
                    <div class="col-sm-9">
                        <input type="text" name="nama_kelas_ed" class="form-control" id="nama_kelas" placeholder="Masukkan Nama Kelas Disini" required="">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="level" class="col-sm-3 text-right control-label col-form-label">Level Kelas</label>
                    <div class="col-sm-9">
                        <input type="text" name="level_ed" class="form-control" id="level" placeholder="Masukkan Level Kelas Disini" required="">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-9">
                        <button type="submit" class="btn btn-primary"> Simpan</button>
                    </div>
                </div>

            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>

<!-- <div class="modal fade" id="addkelas" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Data Kelas</h5>
                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{route('kelas.store')}}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label>Nama Kelas</label>
                                <input type="text" name="nama_kelas" id="nama_kelas">
                            </div>
                        </div>
        
                        <div class="col-12">
                            <div class="form-group">
                                <label>Tingkat</label>
                                <input type="text" name="tingkat" id="tingkat">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer ">
                        <button type="submit" class="btn btn-submit">Confirm</button>
                        <button type="button" class="btn btn-cancel" data-bs-dismiss="modal">Cancel</button>
                    </div>
                </form>
        </div>
    </div>
</div>
    
@endsection -->