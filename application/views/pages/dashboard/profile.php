<div class="page-wrapper">
    <div class="content">
        <div class="page-header">
            <div class="page-title">
                <h4>Profile</h4>
                <h6>User Profile</h6>
            </div>
        </div>

        <div class="card">
            <div class="card-body">
                <div class="profile-set">
                    <div class="profile-head">
                    </div>
                    <form action="<?php echo base_url('ubah/datauser') ?>" method="POST" enctype="multipart/form-data">
                        <div class="profile-top">
                            <div class="profile-content">
                                <div class="profile-contentimg">
                                    <?php if($user[0]['image'] != ""){ ?>
                                        <img src="<?= base_url('uploads/'); echo $user[0]['image'] ?>" alt="img" id="blah">
                                    <?php }else{?>
                                        <img src="<?= base_url() ?>assets/images/default.png" alt="img" id="blah">
                                    <?php } ?>
                                    <div class="profileupload">
                                        <input type="file" id="imgInp" name="image">
                                        <a href="javascript:void(0);"><img src="<?= base_url() ?>assets/assets/img/icons/edit-set.svg" alt="img"></a>
                                    </div>
                                </div>
                                <div class="profile-contentname">
                                    <h2>@<?= $user[0]['xUser'] ?></h2>
                                    <h4><?= $user[0]['nama'] ?></h4>
                                </div>
                            </div>
                        </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-sm-12">
                                <div class="form-group">
                                    <label>Nama</label>
                                    <input type="text" name="nama" value="<?= $user[0]['nama'] ?>">
                                </div>
                            </div>
                            <div class="col-lg-6 col-sm-12">
                                <div class="form-group">
                                    <label>Email</label>
                                    <input type="text" name="email" value="<?= $user[0]['email'] ?>">
                                </div>
                            </div>
                            <div class="col-lg-6 col-sm-12">
                                <div class="form-group">
                                    <label>User Name</label>
                                    <input type="text" name="xUser" value="<?= $user[0]['xUser'] ?>">
                                </div>
                            </div>
                            <div class="col-lg-6 col-sm-12">
                                <div class="form-group">
                                    <label>Password</label>
                                    <div class="pass-group">
                                        <input type="password" class="pass-input" name="xPass">
                                        <span class="fas toggle-password fa-eye-slash"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12">
                                <button type="submit" class="btn btn-submit me-2">Simpan</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>