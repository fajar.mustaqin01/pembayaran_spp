<div class="page-wrapper">
    
    <div class="content container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col">
                    <h3 class="page-title">Data Biaya</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url('admin')?>">Beranda</a></li>
                        <li class="breadcrumb-item active">Data Biaya</li>
                    </ul>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Data Biaya</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-top">
                            <div class="search-set">
                                <div class="search-input">
                                    <a class="btn btn-searchset"><img src="<?= base_url('assets/assets/img/icons/search-white.svg') ?>" alt="img"></a>
                                </div>
                            </div>
                            <div class="wordset">
                                <ul>
                                    <li>
                                        <a type="button" data-bs-toggle="modal" data-bs-target="#addbiaya" class="btn btn-primary"><img src="<?= base_url('assets/assets/img/icons/plus.svg') ?>" alt="img"> Tambah Biaya</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table datanew" id="zero_config">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Jenis Pembayaran</th>
                                        <th>Kode Pembayaran</th>
                                        <th>Deskripsi</th>
                                        <th>Status</th>
                                        <th>Jumlah</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $nom = 0; $no = 1; foreach ($biaya1 as $bia1): ?>
                                        <tr>
                                            <td><?php echo $no++; ?></td>
                                            <td><?php echo $bia1->jenis ?></td>
                                            <td><?php echo $bia1->kode ?></td>
                                            <td><?php echo $bia1->deskripsi ?></td>
                                            <td><?php echo $bia1->status ?></td>
                                            <td style="text-align: right"><span style="float: left">Rp. </span><?php echo rupiah($bia1->jumlah) ?></td>
                                            <td>
                                                <a class="me-3 ubah_biaya_atas" type="button" data-id="<?php echo $bia1->id_biaya ?>">
                                                    <img src="<?= base_url('assets/assets/img/icons/edit.svg') ?>" alt="img">
                                                </a>
                                                <a href="<?php echo base_url('hapus/biaya/').$bia1->id_biaya ?>" onclick="return confirm('Anda akan menghapus data <?php echo $bia1->deskripsi ?>')">
                                                    <img src="<?= base_url('assets/assets/img/icons/delete.svg') ?>" alt="img">
                                                </a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>    
    </div>
</div>

        <!-- Modal -->
<div class="modal fade" id="addbiaya" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Data Biaya</h5>
                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php echo form_open('tambah/biaya'); ?>
            <div class="modal-body">
                <div class="form-group row">
                    <label for="jenis" class="col-sm-3 text-right control-label col-form-label">Jenis</label>
                    <div class="col-sm-9">
                        <input type="text" name="jenis" class="form-control" id="jenis" placeholder="Masukkan Jenis Pembayaran" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="kode" class="col-sm-3 text-right control-label col-form-label">Kode</label>
                    <div class="col-sm-9">
                        <input type="text" name="kode" class="form-control" id="kode" placeholder="Masukkan Kode Pembayaran" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="deskripsi" class="col-sm-3 text-right control-label col-form-label">Deskripsi</label>
                    <div class="col-sm-9">
                        <textarea name="deskripsi" id="deskripsi" rows="3" class="form-control" placeholder="Masukkan Deskripsi Pembayaran Disini"></textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="jumlah" class="col-sm-3 text-right control-label col-form-label">Jumlah</label>
                    <div class="col-sm-9">
                        <input type="number" name="jumlah" class="form-control" id="jumlah" placeholder="Masukkan Jumlah Pembayaran Disini" required>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-9">
                        <button type="submit" class="btn btn-primary"> Simpan</button>
                    </div>
                </div>

            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div> 

<div class="modal fade" id="modal_ubah_biaya_atas" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Data Biaya</h5>
                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?php echo base_url('ubah/biaya') ?>" method="post" id="form_ubah_biaya">
            <input type="hidden" name="id" >
            <div class="modal-body">
                <div class="form-group row">
                    <label for="jenis" class="col-sm-3 text-right control-label col-form-label">Jenis</label>
                    <div class="col-sm-9">
                        <input type="text" name="jenis" class="form-control" id="jenis" placeholder="Masukkan Jenis Pembayaran" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="kode" class="col-sm-3 text-right control-label col-form-label">Kode</label>
                    <div class="col-sm-9">
                        <input type="text" name="kode" class="form-control" id="kode" placeholder="Masukkan Kode Pembayaran" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="deskripsi" class="col-sm-3 text-right control-label col-form-label">Deskripsi</label>
                    <div class="col-sm-9">
                        <textarea name="deskripsi" id="deskripsi" rows="3" class="form-control" placeholder="Masukkan Deskripsi Pembayaran Disini"></textarea>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="jumlah" class="col-sm-3 text-right control-label col-form-label">Jumlah</label>
                    <div class="col-sm-9">
                        <input type="number" name="jumlah" class="form-control" id="jumlah" placeholder="Masukkan Jumlah Pembayaran Disini" required>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-9">
                        <button type="submit" class="btn btn-primary"> Simpan</button>
                    </div>
                </div>

            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>