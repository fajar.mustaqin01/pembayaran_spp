

$(document).ready(function(){
  
 modal_edit_atas = $('#modal_ubah_biaya_atas');


 $('#zero_config').on('click','.ubah_biaya_atas', function(){
    id = $(this).data('id');

      $.ajax({
        url : '<?= base_url("get/ubahbiaya");?>',
        type : 'post',
        dataType: 'json',
        data : {id: id},
        success: function(data) {
            $('#form_ubah_biaya [name="id"]').val(data[0].id_);
            $('#form_ubah_biaya [name="jenis"]').val(data[0].jenis);
            $('#form_ubah_biaya [name="kode"]').val(data[0].kode);
            $('#form_ubah_biaya [name="deskripsi"]').val(data[0].deskripsi);
            $('#form_ubah_biaya [name="jumlah"]').val(data[0].jumlah);
            $('#form_ubah_biaya [name="kategori"]').val(data[0].kategori);

            modal_edit_atas.modal('show').on('shown.bs,modal');
        },
        error: function(data) {
            alert('error');
        }
    });
  });

});