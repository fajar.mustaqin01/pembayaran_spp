$(document).ready(function(){

    modal_edit_kelas = $('#modal-edit-kelas');


    $('#zero_config').on('click','.ubah-data-kelas', function(){
        id = $(this).data('id');

        $.ajax({
            url : "<?= base_url('get/ubahkelas');?>",
            type : 'post',
            dataType: 'json',
            data : {id: id},
            success: function(data) {
                $('#form_ubah_data_kelas [name="id_kelas"]').val(data[0].id_kelas);
                $('#form_ubah_data_kelas [name="kode_kelas"]').val(data[0].kode_kelas);
                $('#form_ubah_data_kelas [name="nama_kelas"]').val(data[0].nama_kelas);

                modal_edit_kelas.modal('show').on('shown.bs,modal');
            },
            error: function(data) {
                // alert('error');
            }
        });
    });

});