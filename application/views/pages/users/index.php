<div class="page-wrapper">
    
    <div class="content container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col">
                    <h3 class="page-title">Pengguna</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url('admin')?>">Beranda</a></li>
                        <li class="breadcrumb-item active">Pengguna</li>
                    </ul>
                </div>
            </div>
            <div class="page-btn">
                <a type="button" data-bs-toggle="modal" data-bs-target="#adduser" class="btn btn-primary"><img src="<?= base_url('assets/assets/img/icons/plus.svg') ?>" alt="img"> Tambah Pengguna</a>
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Data Pengguna Sistem</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-top">
                            <div class="search-set">
                                <div class="search-input">
                                    <a class="btn btn-searchset"><img src="<?= base_url('assets/assets/img/icons/search-white.svg') ?>" alt="img"></a>
                                </div>
                            </div>
                            <div class="wordset">
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered datanew" id="zero_config">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>Username</th>
                                        <th>Email</th>
                                        <th>Role</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no = 1; foreach ($users as $user): ?>
                                        <tr>
                                            <td><?php echo $no++; ?></td>
                                            <td><?php echo $user->nama ?></td>
                                            <td><?php echo $user->xUser ?></td>
                                            <td><?php echo $user->email ?></td>
                                            <td>
                                                <?php 
                                                if ($user->level == '1') {
                                                    echo 'Admin';
                                                } elseif ($user->level == '2') {
                                                    echo 'Staff TU';
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <a class="me-3 ubah-data-user" type="button" data-id="<?php echo $user->id ?>">
                                                    <img src="<?= base_url('assets/assets/img/icons/edit.svg') ?>" alt="img">
                                                </a>
                                                <a href="<?php echo base_url('hapus/user/').$user->id ?>"  onclick="return confirm('Anda akan menghapus data User <?php echo $user->nama ?>')">
                                                    <img src="<?= base_url('assets/assets/img/icons/delete.svg') ?>" alt="img">
                                                </a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
    </div>
</div>

        <!-- Modal -->
<div class="modal fade" id="adduser" tabindex="-1" level="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" level="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Pengguna</h5>
                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php echo form_open('tambah/user'); ?>
                    <div class="modal-body">
                        <div class="form-group row">
                            <label for="nama" class="col-sm-3 text-right control-label col-form-label">Nama Pengguna <sup class="text-danger">*</sup></label>
                            <div class="col-sm-9">
                                <input type="text" name="nama" class="form-control" id="nama" placeholder="Masukkan Nama" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="username" class="col-sm-3 text-right control-label col-form-label">Username <sup class="text-danger">*</sup></label>
                            <div class="col-sm-9">
                                <input type="text" name="Username" class="form-control" id="Username" placeholder="Masukkan Username Disini" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="email" class="col-sm-3 text-right control-label col-form-label">Email <sup class="text-danger">*</sup></label>
                            <div class="col-sm-9">
                                <input type="email" name="email" class="form-control" id="email" placeholder="Masukkan email Disini" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="password" class="col-sm-3 text-right control-label col-form-label">Password Default</label>
                            <div class="col-sm-9">
                                <input type="text" value="12345" disabled required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="level" class="col-sm-3 text-right control-label col-form-label">level <sup class="text-danger">*</sup></label>
                            <div class="col-sm-9">
                                <select name="level" id="level" class="form-control" required>
                                    <option value="">-- Pilih level --</option>
                                    <option value="2">Staff TU</option>
                                    <!-- <option value="3">Siswa</option> -->
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-9">
                                <button type="submit" class="btn btn-primary"> Simpan</button>
                            </div>
                        </div>

                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>


<!-- Modal -->
<div class="modal fade" id="modal_ubah_data_user" tabindex="-1" level="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" level="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Ubah Data Pengguna</h5>
                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?php echo base_url('ubah/datauser_admin') ?>" method="post" id="form_ubah_data_user">
            <div class="modal-body">
                <div class="form-group row">
                    <label for="username" class="col-sm-3 text-right control-label col-form-label">Username</label>
                    <div class="col-sm-9">
                        <input type="text" name="username" class="form-control" id="username" readonly>
                        <input type="hidden" name="id" />
                    </div>
                </div>
                <div class="form-group row">
                    <label for="nama" class="col-sm-3 text-right control-label col-form-label">Nama</label>
                    <div class="col-sm-9">
                        <input type="text" name="nama" class="form-control" id="nama" required>
                        <input type="hidden" name="id" />
                    </div>
                </div>
                <div class="form-group row">
                    <label for="email" class="col-sm-3 text-right control-label col-form-label">email</label>
                    <div class="col-sm-9">
                        <input type="email" name="email" class="form-control" id="email" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="level" class="col-sm-3 text-right control-label col-form-label">level</label>
                    <div class="col-sm-9">
                        <input type="text" name="level" class="form-control" id="level" readonly>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-9">
                        <button type="submit" class="btn btn-primary"> Simpan</button>
                    </div>
                </div>

            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>