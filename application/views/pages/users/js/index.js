$(document).ready(function(){

        modal_edit_siswa = $('#modal_ubah_data_user');


        $('#zero_config').on('click','.ubah-data-user', function(){
        id = $(this).data('id');

            //  modal_edit_siswa.modal('show').on('shown.bs,modal');
            var level = "";

            $.ajax({
            url : "<?= base_url('get/ubahuser')?>",
            type : 'post',
            dataType: 'json',
            data : {id: id},
            success: function(data) {
                $('#form_ubah_data_user [name="id"]').val(data[0].id);
                $('#form_ubah_data_user [name="nama"]').val(data[0].nama);
                $('#form_ubah_data_user [name="username"]').val(data[0].xUser);
                $('#form_ubah_data_user [name="email"]').val(data[0].email);

                if(data[0].level == '1'){
                    level = "Admin";
                }else if(data[0].level == '2'){
                    level = "Staff TU";
                }
                
                $('#form_ubah_data_user [name="level"]').val(level);

                modal_edit_siswa.modal('show').on('shown.bs,modal');
            },
            error: function(data) {
                alert('error');
            }
        });
        });

});