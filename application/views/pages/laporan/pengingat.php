v<div class="page-wrapper">
    
    <div class="content container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col">
                    <h3 class="page-title">Pengingat Pembayaran</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url('admin')?>">Beranda</a></li>
                        <li class="breadcrumb-item active">Pengingat Pembayaran</li>
                    </ul>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Data Status SPP Siswa Berjalan</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-top">
                            <div class="search-set">
                                <div class="search-input">
                                    <a class="btn btn-searchset"><img src="<?= base_url('assets/assets/img/icons/search-white.svg') ?>" alt="img"></a>
                                </div>
                            </div>
                            <div class="wordset">
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table datanew" id="zero_config">
                                <thead class="bg-dark">
                                    <tr>
                                        <th class="text-white">No</th>
                                        <th class="text-white">NISN</th>
                                        <th class="text-white">NAMA</th>
                                        <th class="text-white">Kelas</th>
                                        <th class="text-white">Jumlah Pembayaran</th>
                                        <th class="text-white">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $nom = 0; $no = 1; $status = ''; foreach ($siswa as $kls): 
                                        
                                        if($kls->total_pembayaran >= $biaya_spp[0]['jumlah'] ){
                                            $status = '<span class="badges bg-lightgreen">Lunas</span>';
                                        }elseif($kls->total_pembayaran > "0"){
                                            $status = '<span class="badges bg-warning">Belum Lunas</span>';
                                        }else{
                                            $status = '<span class="badges bg-danger">Belum Bayar</span>';
                                        }
                                    ?>
                                        
                                        <tr>
                                            <td><?php echo $no++; ?></td>
                                            <td><?php echo $kls->nisn ?></td>
                                            <td><?php echo $kls->nama ?></td>
                                            <td><?php echo $kls->nama_kelas ?></td>
                                            <td><?php echo $kls->total_pembayaran ?></td>
                                            <td>
                                            <?php 
                                                echo $status . " ";
                                                if($kls->total_pembayaran < $biaya_spp[0]['jumlah']){
                                                    echo '<a href="'. base_url() . 'email/sendEmail?id='.$kls->id_siswa.'" class="btn btn-warning btn-sm">Send Email</a>';
                                                }
                                            
                                            ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
    </div>
</div>