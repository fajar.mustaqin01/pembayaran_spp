v<div class="page-wrapper">
    
    <div class="content container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col">
                    <h3 class="page-title">Laporan Keuangan</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url('admin')?>">Beranda</a></li>
                        <li class="breadcrumb-item active">Laporan Keuangan</li>
                    </ul>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Laporan Pembayaran SPP</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-top">
                            <div class="search-set">
                                <div class="row">
                                    <div class="col-sm-4 mb-2">
                                        <label for="">Tanggal Awal:</label><br>
                                        <input type="date" value="<?= $tanggal ?>" id="dateFr" name="dateFr" data-date-format="dd-mm-yyyy" class="form-control" data-provide="datepicker" data-date-autoclose="true" required>
                                    </div>
                                    <div class="col-sm-4 mb-2">
                                        <label for="">Tanggal Akhir:</label><br>
                                        <input type="date" value="<?= $tanggal?>" id="dateTo" name="dateTo" data-date-format="dd-mm-yyyy" class="form-control" data-provide="datepicker" data-date-autoclose="true" required>
                                    </div>
                                    <div class="col-sm-4 mb-2">
                                        <br>
                                        <button type="button" class="btn btn-success" onclick="carifilter()"><i class="fa fa-search"></i> Tampilkan</button>
                                    </div>
                                </div>
                            </div>
                            <div class="wordset">
                                <!-- <ul>
                                    <li>
                                        <a data-bs-toggle="tooltip" data-bs-placement="top" title="pdf"><img src="assets/img/icons/pdf.svg" alt="img"></a>
                                    </li>
                                    <li>
                                        <a data-bs-toggle="tooltip" data-bs-placement="top" title="excel"><img src="assets/img/icons/excel.svg" alt="img"></a>
                                    </li>
                                    <li>
                                        <a data-bs-toggle="tooltip" data-bs-placement="top" title="print"><img src="assets/img/icons/printer.svg" alt="img"></a>
                                    </li>
                                </ul> -->
                            </div>
                        </div>

                        <div class="container">
                            <h4 class="text-center font-weight-bolder text-bold text-uppercase">laporan pembayaran spp</h4>
                            <p class="text-center">Periode: <b id="tanggal-awal"><?= $tanggal_indo ?></b> - <b id="tanggal-akhir"><?= $tanggal_indo ?></b></p>
                            <div id="laporan">
                                <div id="container-data"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
    </div>
</div>