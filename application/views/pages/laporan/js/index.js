$(function () {
    var tanggal_awal = "<?= $tanggal ?>";
    var tanggal_akhir = "<?= $tanggal ?>";

    var dateFr = tanggal_awal;
    var dateTo = tanggal_akhir;

    loadReport(dateFr, dateTo);

});

function carifilter() {
    var tanggal_awal = $('#dateFr').val();
    var tanggal_akhir = $('#dateTo').val();

    $('#link-cetak').attr('href', site_url + 'reportKasMasjid/pdf_kasMasjidMasuk?dateFr=' + tanggal_awal + '&dateTo=' + tanggal_akhir);
    if (tanggal_akhir >= tanggal_awal) {
        loadReport(tanggal_awal, tanggal_akhir);
    } else {
        Swal.fire('Tanggal', 'Tanggal akhir tidak boleh kurang dari tanggal awal', 'warning');
    }
}

function loadReport(tanggal_awal, tanggal_akhir) {
    $.ajax({
        url: site_url + '/admin/laporanspp_data',
        type: "POST",
        data: {
            dateFr: tanggal_awal,
            dateTo: tanggal_akhir
        },
        dataType: "JSON",
        success: function (response) {
            $('#tanggal-awal').text(response.tanggal_awal);
            $('#tanggal-akhir').text(response.tanggal_akhir);
            $('#container-data').html(response.data);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            Swal.fire('Error!', 'Internal server error', 'error').then((result) => {
            });
        }
    });
}