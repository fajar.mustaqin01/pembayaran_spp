<div class="page-wrapper">
    
    <div class="content container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col">
                    <h3 class="page-title">Siswa</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url('admin')?>">Beranda</a></li>
                        <li class="breadcrumb-item active">Siswa</li>
                    </ul>
                </div>
            </div>
            <div class="page-btn">
                <a type="button" data-bs-toggle="modal" data-bs-target="#addsiswa" class="btn btn-primary"><img src="<?= base_url('assets/assets/img/icons/plus.svg') ?>" alt="img"> Tambah Siswa</a>
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Data Siswa TK</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-top">
                            <div class="search-set">
                                <div class="search-input">
                                    <a class="btn btn-searchset"><img src="<?= base_url('assets/assets/img/icons/search-white.svg') ?>" alt="img"></a>
                                </div>
                            </div>
                            <div class="wordset">
                                <!-- <ul>
                                    <li>
                                        <a data-bs-toggle="tooltip" data-bs-placement="top" title="pdf"><img src="assets/img/icons/pdf.svg" alt="img"></a>
                                    </li>
                                    <li>
                                        <a data-bs-toggle="tooltip" data-bs-placement="top" title="excel"><img src="assets/img/icons/excel.svg" alt="img"></a>
                                    </li>
                                    <li>
                                        <a data-bs-toggle="tooltip" data-bs-placement="top" title="print"><img src="assets/img/icons/printer.svg" alt="img"></a>
                                    </li>
                                </ul> -->
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-bordered datanew" id="zero_config">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Siswa</th>
                                        <th>Email</th>
                                        <th>NISN</th>
                                        <th>JK</th>
                                        <th>TTL</th>
                                        <th>Nama Kelas</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $no = 1; foreach ($siswa as $siswa): 
                                        $email = "-";

                                        if($siswa->email != ''){
                                            $email = $siswa->email;
                                        }
                                        ?>
                                        <tr>
                                            <td><?php echo $no++; ?></td>
                                            <td><?php echo $siswa->nama ?></td>
                                            <td><?php echo $email ?></td>
                                            <td><?php echo $siswa->nisn ?></td>
                                            <td>
                                                <?php 
                                                if ($siswa->jk == '1') {
                                                    echo 'Laki-laki';
                                                } elseif ($siswa->jk == '2') {
                                                    echo 'Perempuan';
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <?php echo $siswa->tempat_lahir.', '.tgl_indo($siswa->tg_lahir); ?>
                                            </td>
                                            <td><?php echo $siswa->nama_kelas ?></td>
                                            <td>
                                                <a class="me-3 ubah-data-siswa" type="button" data-id="<?php echo $siswa->id_siswa ?>">
                                                    <img src="<?= base_url('assets/assets/img/icons/edit.svg') ?>" alt="img">
                                                </a>
                                                <a href="<?php echo base_url('hapus/siswa/').$siswa->id_siswa ?>"  onclick="return confirm('Anda akan menghapus data siswa <?php echo $siswa->nama ?>')">
                                                    <img src="<?= base_url('assets/assets/img/icons/delete.svg') ?>" alt="img">
                                                </a>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    
    </div>
</div>

        <!-- Modal -->
<div class="modal fade" id="addsiswa" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Tambah Data Kelas</h5>
                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php echo form_open('tambah/siswa'); ?>
                    <div class="modal-body">
                        <div class="form-group row">
                            <label for="nama" class="col-sm-3 text-right control-label col-form-label">Nama Siswa</label>
                            <div class="col-sm-9">
                                <input type="text" name="nama" class="form-control" id="nama" placeholder="Masukkan Nama Siswa Disini" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="nisn" class="col-sm-3 text-right control-label col-form-label">Nisn</label>
                            <div class="col-sm-9">
                                <input type="number" name="nisn" class="form-control" id="nisn" placeholder="Masukkan NISN Disini" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="email" class="col-sm-3 text-right control-label col-form-label">Email</label>
                            <div class="col-sm-9">
                                <input type="email" name="email" class="form-control" id="email" placeholder="Masukkan Email disini" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="jk" class="col-sm-3 text-right control-label col-form-label">JK</label>
                            <div class="col-sm-9">
                                <select name="jk" id="jk" class="form-control" required>
                                    <option value="">-- Pilih Jenis Kelamin --</option>
                                    <option value="1">Laki-laki</option>
                                    <option value="2">Perempuan</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="tempat_lahir" class="col-sm-3 text-right control-label col-form-label">Tmp Lahir</label>
                            <div class="col-sm-9">
                                <input type="text" name="tempat_lahir" class="form-control" id="tempat_lahir" placeholder="Masukkan Tempat Lahir Disini" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="tg_lahir" class="col-sm-3 text-right control-label col-form-label">Tg Lahir</label>
                            <div class="col-sm-9">
                                <input type="date" name="tg_lahir" class="form-control" placeholder="Pilih Tanggal" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="id_kelas" class="col-sm-3 text-right control-label col-form-label">Kelas</label>
                            <div class="col-sm-9">
                                <select name="id_kelas" id="id_kelas" class="form-select" required>
                                    <option value="">-- Pilih kelas --</option>
                                    <?php foreach ($kelas as $k): ?>
                                        <option value="<?php echo $k->id_kelas ?>"><?php echo $k->nama_kelas ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-9">
                                <button type="submit" class="btn btn-primary"> Simpan</button>
                            </div>
                        </div>

                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>


<!-- Modal -->
<div class="modal fade" id="modal_ubah_data_siswa" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Ubah Data Kelas</h5>
                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?php echo base_url('ubah/datasiswa') ?>" method="post" id="form_ubah_data_siswa">
            <div class="modal-body">
                <div class="form-group row">
                    <label for="nama" class="col-sm-3 text-right control-label col-form-label">Nama Siswa</label>
                    <div class="col-sm-9">
                        <input type="text" name="nama" class="form-control" id="nama" placeholder="Masukkan Nama Siswa Disini" required>
                        <input type="hidden" name="id"/>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="nisn" class="col-sm-3 text-right control-label col-form-label">Nisn</label>
                    <div class="col-sm-9">
                        <input type="number" name="nisn" class="form-control" id="nisn" placeholder="Masukkan NISN Disini" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="email" class="col-sm-3 text-right control-label col-form-label">Email</label>
                    <div class="col-sm-9">
                        <input type="email" name="email" class="form-control" id="email" placeholder="Masukkan Email disini" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="jk" class="col-sm-3 text-right control-label col-form-label">JK</label>
                    <div class="col-sm-9">
                        <select name="jk" id="jk" class="form-select" required>
                            <option value="">-- Pilih Jenis Kelamin --</option>
                            <option value="1">Laki-laki</option>
                            <option value="2">Perempuan</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="tempat_lahir" class="col-sm-3 text-right control-label col-form-label">Tmp Lahir</label>
                    <div class="col-sm-9">
                        <input type="text" name="tempat_lahir" class="form-control" id="tempat_lahir" placeholder="Masukkan Tempat Lahir Disini" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="tg_lahir" class="col-sm-3 text-right control-label col-form-label">Tg Lahir</label>
                    <div class="col-sm-9">
                        <input type="date" name="tg_lahir" class="form-control" placeholder="Pilih Tanggal" required>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="id_kelas" class="col-sm-3 text-right control-label col-form-label">Kelas</label>
                    <div class="col-sm-9">
                        <select name="id_kelas" id="id_kelas" class="form-select" required>
                            <option value="">-- Pilih kelas --</option>
                            <?php foreach ($kelas as $k): ?>
                                <option value="<?php echo $k->id_kelas ?>"><?php echo $k->nama_kelas ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-9">
                        <button type="submit" class="btn btn-primary"> Simpan</button>
                    </div>
                </div>

            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>