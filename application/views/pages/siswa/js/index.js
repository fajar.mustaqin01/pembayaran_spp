
$(document).ready(function(){

        modal_edit_siswa = $('#modal_ubah_data_siswa');


        $('#zero_config').on('click','.ubah-data-siswa', function(){
        id = $(this).data('id');

            //  modal_edit_siswa.modal('show').on('shown.bs,modal');

            $.ajax({
            url : "<?= base_url('get/ubahsiswa')?>",
            type : 'post',
            dataType: 'json',
            data : {id: id},
            success: function(data) {
                $('#form_ubah_data_siswa [name="id"]').val(data[0].id_siswa);
                $('#form_ubah_data_siswa [name="nama"]').val(data[0].nama);
                $('#form_ubah_data_siswa [name="nisn"]').val(data[0].nisn);
                $('#form_ubah_data_siswa [name="email"]').val(data[0].email);
                $('#form_ubah_data_siswa [name="jk"]').val(data[0].jk);
                $('#form_ubah_data_siswa [name="tempat_lahir"]').val(data[0].tempat_lahir);
                $('#form_ubah_data_siswa [name="tg_lahir"]').val(data[0].tg_lahir);
                $('#form_ubah_data_siswa [name="id_kelas"]').val(data[0].kelas_id).trigger('change');

                modal_edit_siswa.modal('show').on('shown.bs,modal');
            },
            error: function(data) {
                alert('error');
            }
        });
        });

});