<div class="page-wrapper">
    
    <div class="content container-fluid">
        <div class="page-header">
            <div class="row">
                <div class="col">
                    <h3 class="page-title">Pembayaran SPP</h3>
                    <ul class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url('admin')?>">Beranda</a></li>
                        <li class="breadcrumb-item active">Pembayaran SPP</li>
                    </ul>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header bg-success">
                        <h4 class="card-title text-white">Formulir Pembayaran SPP</h4>
                    </div>
                        <?php echo form_open('tambah/inputPembayaranNon'); ?>
                            <div class="card-body">
                                <div class="form-group row">
                                    <label for="nama_instansi" class="col-sm-12 text-right control-label col-form-label">Cari Siswa</label>
                                    <div class="col-sm-12">
                                        <select name="siswa" id="siswa" class="select2 form-control" required style="width: 100%">
                                            <option value="">-- Pilih Peserta Didik --</option>
                                            <?php foreach ($siswa as $sis): ?>
                                                <option value="<?= $sis->nisn ?>"><?= $sis->nama.' ( '.$sis->nama_kelas.' )' ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group" id="pilih" style="display: none">
                                    <label for="" class="col-sm-12 text-center"><h4>Data Pembayaran</h4></label>
                                    <div class="col-sm-12" id="tampil_pilihan_non">
                                        
                                    </div>
                                </div>
                                
                            </div>
                            <div class="card-footer">
                                <button type="submit" class="btn btn-primary">Simpan</button>
                            </div>
                        <?php echo form_close(); ?>
                </div>
                <?php echo $this->session->flashdata('cetak'); ?>
            </div>
        </div>
    
    </div>
</div>



