<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

function tgl_indo($tanggal)
{
	$th = substr($tanggal, 0, 4);
	$bln = substr($tanggal, 5, 2);
	$tgl = substr($tanggal, 8, 2);

	return $tgl.' - '.$bln.' - '.$th;
}

function tanggal_indo($tanggal) {
    $tanggalArray = explode('-', $tanggal);
    if (count($tanggalArray) == 3) {
        $bulanIndo = [
            '01' => 'Januari',
            '02' => 'Februari',
            '03' => 'Maret',
            '04' => 'April',
            '05' => 'Mei',
            '06' => 'Juni',
            '07' => 'Juli',
            '08' => 'Agustus',
            '09' => 'September',
            '10' => 'Oktober',
            '11' => 'November',
            '12' => 'Desember'
        ];

        $tahun = $tanggalArray[2];
        $bulan = $bulanIndo[$tanggalArray[1]];
        $tanggal = $tanggalArray[0];

        return $tanggal . ' ' . $bulan . ' ' . $tahun;
    }

    return $tanggal; // Mengembalikan tanggal tanpa perubahan jika format tidak sesuai.
}

function bulan($bln)
{
	if ($bln == '01') {
		return 'Januari';
	} elseif ($bln == '02') {
		return 'Februari';
	} elseif ($bln == '03') {
		return 'Maret';
	} elseif ($bln == '04') {
		return 'April';
	} elseif ($bln == '05') {
		return 'Mei';
	} elseif ($bln == '06') {
		return 'Juni';
	} elseif ($bln == '07') {
		return 'Juli';
	} elseif ($bln == '08') {
		return 'Agustus';
	} elseif ($bln == '09') {
		return 'September';
	} elseif ($bln == '10') {
		return 'Oktober';
	} elseif ($bln == '11') {
		return 'November';
	} elseif ($bln == '12') {
		return 'Desember';
	}
	
}