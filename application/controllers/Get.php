<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Get extends CI_Controller {

	function jenis()
	{
		// $nisn = $this->input->post('nisn');

		// $kategori = $this->db->get_where('tbl_siswa',['nisn' => $nisn])->row_array()['kategori'];

		$data = $this->db->get_where('tbl_biaya', ['jenis' => 'SPP'])->row_array();

		$tampil = '<div class="form-group"><label>Pilih Bulan</label><br>';
		$tampil .= '<select class="form-control" name="bulan" required>';
		$tampil .= '<option value="" selected disabled>==Pilih Bulan==</option>';
		$tampil .= '<option value="01">Januari</option>';
		$tampil .= '<option value="02">Februari</option>';
		$tampil .= '<option value="03">Maret</option>';
		$tampil .= '<option value="04">April</option>';
		$tampil .= '<option value="05">Mei</option>';
		$tampil .= '<option value="06">Juni</option>';
		$tampil .= '<option value="07">Juli</option>';
		$tampil .= '<option value="08">Agustus</option>';
		$tampil .= '<option value="09">September</option>';
		$tampil .= '<option value="10">Oktober</option>';
		$tampil .= '<option value="11">November</option>';
		$tampil .= '<option value="12">Desember</option>';
		$tampil .= '</select></div>';
		// $tampil .= '<input type="radio" name="bulan" value="01" required>&nbsp;<label>Januari</label>';
		// $tampil .= '<input type="radio" name="bulan" value="02" style="margin-left:15px" required>&nbsp;<label>Februari</label>';
		// $tampil .= '<input type="radio" name="bulan" value="03" style="margin-left:15px" required>&nbsp;<label>Maret</label>';
		// $tampil .= '<input type="radio" name="bulan" value="04" style="margin-left:15px" required>&nbsp;<label>April</label>';
		// $tampil .= '<input type="radio" name="bulan" value="05" style="margin-left:15px" required>&nbsp;<label>Mei</label>';
		// $tampil .= '<input type="radio" name="bulan" value="06" style="margin-left:15px" required>&nbsp;<label>Juni</label>';
		// $tampil .= '<input type="radio" name="bulan" value="07" style="margin-left:15px" required>&nbsp;<label>Juli</label>';
		// $tampil .= '<input type="radio" name="bulan" value="08" style="margin-left:15px" required>&nbsp;<label>Agustus</label>';
		// $tampil .= '<input type="radio" name="bulan" value="09" style="margin-left:15px" required>&nbsp;<label>September</label>';
		// $tampil .= '<input type="radio" name="bulan" value="10" style="margin-left:15px" required>&nbsp;<label>Oktober</label>';
		// $tampil .= '<input type="radio" name="bulan" value="11" style="margin-left:15px" required>&nbsp;<label>November</label>';
		// $tampil .= '<input type="radio" name="bulan" value="12" style="margin-left:15px" required>&nbsp;<label>Desember</label>';

		$tampil .= '<br><br>';
		$tampil .= '<label>Jumlah Dibayar</label><br>';
		$tampil .= '<input type="text" name="jumlah" class="form-control col-md-6 rupiah" required>';
		$tampil .= '<small>Jumlah Seharusnya Rp. '.rupiah($data['jumlah']).'</small>';

		$tampil .= '<input type="hidden" name="seharusnya" value="'.$data['jumlah'].'">';
		$tampil .= '<input type="hidden" name="deskripsi" value="'.$data['deskripsi'].'">';


		echo $tampil;
	}

	function jenis_non()
	{
		$nisn = $this->input->post('nisn');
		
		$siswa = $this->db->get_where('tbl_siswa', ['nisn' => $nisn])->row_array();
	
		if ($siswa !== null) {
			$this->db->where_not_in('jenis', 'SPP');
			$data = $this->db->get('tbl_biaya');
			$query = $data->result();
	
			$tampil = '';
			
			foreach ($query as $key) {
				$tampil .= '<input type="hidden" name="harus[]" value="'.$key->jumlah.'">';
				$tampil .= '<div class="row">';
				$tampil .= '<label>'.$key->jenis.'</label><input type="number" name="'.$key->jenis.'" class="form-control">';
				$tampil .= '<small>Jumlah Seharusnya Rp. '.rupiah($key->jumlah).'</small>';
				$tampil .= '</div>';
				$tampil .= '<br>';
			}
	
			echo $tampil;
		} else {
			echo 'Data siswa tidak ditemukan.';
		}
	}
	

	function ubahkelas()
	{
		$id = $this->input->post('id');

		$data = $this->db->get_where('tbl_kelas',['id_kelas' => $id])->result();
		echo json_encode($data);
	}

	function ubahuser()
	{
		$id = $this->input->post('id');
		$data = $this->db->get_where('tbl_pengguna',['id' => $id])->result();

		echo json_encode($data);
	}

	function ubahsiswa()
	{
		$id = $this->input->post('id');
		$data = $this->db->get_where('tbl_siswa',['id_siswa' => $id])->result();

		echo json_encode($data);
	}

	function ubahbiaya()
	{
		$id = $this->input->post('id');
		$data = $this->db->get_where('tbl_biaya',['id_biaya' => $id])->result();

		echo json_encode($data);
	}


}

/* End of file Get.php */
/* Location: ./application/controllers/Get.php */