<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct(){
		parent::__construct();

		if (! $this->session->userdata('username')) {
			redirect('','refresh');
		}

	}

	public function index()
	{
		$data['jkelas'] = $this->m_admin->data_kelas()->num_rows();
		$data['jsiswa'] = $this->m_admin->jumlah_siswa();
		$data['jpembayaran'] = $this->m_admin->jenis_pembayaran();
		$data['juang'] = $this->m_admin->jum_uang();

		$data['title'] = "Beranda";
		$data['menu'] = "beranda";

		$content_data['jkelas'] = $data['jkelas'];
		$content_data['jsiswa'] = $data['jsiswa'];
		$content_data['jpembayaran'] = $data['jpembayaran'];
		$content_data['juang'] = $data['juang'];

		$data['content'] = $this->load->view('pages/dashboard/index', $content_data, true);

		$this->load->view('template/app', $data);
	}

	public function profile()
	{
		$id_user = $this->session->userdata('id');
		$data['user'] = $this->m_admin->data_user($id_user);		

		$data['title'] = "Profile";

		$content_data['user'] = $data['user'];
		$content_data['menu'] = '';

		$data['content'] = $this->load->view('pages/dashboard/profile', $content_data, true);

		$this->load->view('template/app', $data);
	}

	public function kelas()
	{
		if (! $this->session->userdata('username')) {
			redirect('','refresh');
		}

		$data['kelas'] = $this->m_admin->data_kelas()->result();

		/* Hitung jumlah siswa masing-masing kelas */
		$j = array();
		foreach ($data['kelas'] as $dt) {
			array_push($j, $this->db->get_where('tbl_siswa', ['kelas_id' => $dt->id_kelas])->num_rows());
		}
		$data['jumlah'] = $j;

		$data['title'] = "Kelas";
		$data['menu'] = "kelas";


		$data['scripts'] = ["pages/kelas/js/index.js"];

		$content_data['jumlah'] = $data['jumlah'];
		$content_data['kelas'] = $data['kelas'];

		$data['content'] = $this->load->view('pages/kelas/index', $content_data, true);

		$this->load->view('template/app', $data);

		// $this->load->view('admin/meta');
		// $this->load->view('admin/header');
		// $this->load->view('admin/sidebar');
		// $this->load->view('admin/kelas', $data);
		// $this->load->view('admin/footer');
		// $this->load->view('admin/script');
	}

	public function instansi()
	{
		if (! $this->session->userdata('username')) {
			redirect('','refresh');
		}

		$data['instansi'] = $this->m_admin->data_instansi();

		$this->load->view('admin/meta');
		$this->load->view('admin/header');
		$this->load->view('admin/sidebar');
		$this->load->view('admin/instansi', $data);
		$this->load->view('admin/footer');
		$this->load->view('admin/script');
	}

	public function siswa()
	{
		if (! $this->session->userdata('username')) {
			redirect('','refresh');
		}

		$data['kelas'] = $this->m_admin->data_kelas()->result();

		$data['siswa'] = $this->m_admin->data_siswa()->result();

		$data['title'] = "Siswa";
		$data['menu'] = "siswa";


		$data['scripts'] = ["pages/siswa/js/index.js"];

		$content_data['siswa'] = $data['siswa'];
		$content_data['kelas'] = $data['kelas'];
		// $content_data['sidebar'] = $this->uri->segment(2);

		$data['content'] = $this->load->view('pages/siswa/index', $content_data, true);

		$this->load->view('template/app', $data);

		// $this->load->view('admin/meta');
		// $this->load->view('admin/header');
		// $this->load->view('admin/sidebar');
		// $this->load->view('admin/siswa', $data);
		// $this->load->view('admin/footer');
		// $this->load->view('admin/script');
	}

	public function users()
	{
		if (! $this->session->userdata('username')) {
			redirect('','refresh');
		}

		$data['users'] = $this->m_admin->data_users()->result();

		$data['title'] = "Kelola Pengguna";
		$data['menu'] = "users";


		$data['scripts'] = ["pages/users/js/index.js"];

		$content_data['users'] = $data['users'];

		$data['content'] = $this->load->view('pages/users/index', $content_data, true);

		$this->load->view('template/app', $data);

	}

	public function biaya()
	{
		if (! $this->session->userdata('username')) {
			redirect('','refresh');
		}

		$data['biaya1'] = $this->m_admin->biaya1()->result();

		$data['title'] = "Biaya";
		$data['menu'] = "biaya";


		$data['scripts'] = ["pages/biaya/js/index.js"];

		$content_data['biaya1'] = $data['biaya1'];

		$data['content'] = $this->load->view('pages/biaya/index', $content_data, true);

		$this->load->view('template/app', $data);
	}

	public function spp()
	{
		if (! $this->session->userdata('username')) {
			redirect('','refresh');
		}

		$data['siswa'] = $this->m_admin->data_siswa()->result();

		$data['title'] = "SPP";
		$data['menu'] = "spp";


		$data['scripts'] = ["pages/pembayaran/js/spp.js"];

		$content_data['siswa'] = $data['siswa'];

		$data['content'] = $this->load->view('pages/pembayaran/spp', $content_data, true);

		$this->load->view('template/app', $data);
	}

	public function non_spp()
	{
		if (! $this->session->userdata('username')) {
			redirect('','refresh');
		}

		$data['siswa'] = $this->m_admin->data_siswa()->result();

		$data['title'] = "SPP";
		$data['menu'] = "nonspp";


		$data['scripts'] = ["pages/pembayaran/js/spp.js"];

		$content_data['siswa'] = $data['siswa'];

		$data['content'] = $this->load->view('pages/pembayaran/non_spp', $content_data, true);

		$this->load->view('template/app', $data);
	}

	function cetakbukti($kode_pembayaran)
	{
		$data['kode_'] = $kode_pembayaran;

		$data['instansi'] = $this->m_admin->data_instansi();

		$this->db->where('kode_pembayaran', $kode_pembayaran);
		$dt = $this->db->get('tbl_pembayaran');
		$data['pembayaran'] = $dt->result();

		$data['tgl_bayar'] = $this->db->get_where('tbl_pembayaran',['kode_pembayaran' => $kode_pembayaran])->row_array()['tgl_bayar'];

		$nisn = $dt->row_array()['nisn'];
		$data['siswa'] = $this->db->get_where('tbl_siswa',['nisn' => $nisn])->row_array();

		// jumlah pembayaran
		$jml = $this->db->get_where('tbl_pembayaran', ['kode_pembayaran' => $kode_pembayaran])->result_array();
		$juml = array();
		$hrs = array();
		foreach ($jml as $jumlah) {
			array_push($juml, $jumlah['jumlah']);
			array_push($hrs, $jumlah['seharusnya']);
		}
		$data['jumlah_bayar'] = array_sum($juml);
		$data['harus'] = array_sum($hrs);

		if ($data['jumlah_bayar'] > $data['harus']) {
			$data['sisa'] = $data['jumlah_bayar']-$data['harus'];
		} elseif($data['jumlah_bayar'] < $data['harus']) {
			$data['sisa'] = $data['harus']-$data['jumlah_bayar'];
		} elseif ($data['jumlah_bayar'] == $data['harus']) {
			$data['sisa'] = '0';
		}
			
		$this->load->view('admin/cetakbukti', $data);
	}

	public function laporan()
	{
		if (! $this->session->userdata('username')) {
			redirect('','refresh');
		}

		$data['kelas'] = $this->m_admin->data_kelas()->result();

		$data['siswa'] = $this->m_admin->data_siswa()->result();

		$data['jenis_biaya'] = $this->m_admin->jenis_biaya();

		$this->load->view('admin/meta');
		$this->load->view('admin/header');
		$this->load->view('admin/sidebar');
		$this->load->view('admin/laporan', $data);
		$this->load->view('admin/footer');
		$this->load->view('admin/script');
	}

	public function pengingat()
	{

		$data['siswa'] = $this->m_admin->laporan_keuangan()->result();
		$data['biaya_spp'] = $this->m_admin->biaya_spp()->result_array();

		$data['title'] = "Pengingat Pembayaran";
		$data['menu'] = "pengingat";


		$content_data['siswa'] = $data['siswa'];
		$content_data['biaya_spp'] = $data['biaya_spp'];

		$data['content'] = $this->load->view('pages/laporan/pengingat', $content_data, true);
		
		$this->load->view('template/app', $data);
	}

	public function laporan_spp()
	{
		$data['siswa'] = $this->m_admin->laporan_keuangan()->result();
		$data['biaya_spp'] = $this->m_admin->biaya_spp()->result_array();


		$data['title'] = "Laporan Pembayaran SPP";
		$data['menu'] = "laporan";


		$data['scripts'] = ["pages/laporan/js/index.js"];

		$tanggal = date('Y-m-d');

		$content_data['siswa'] = $data['siswa'];
		$content_data['biaya_spp'] = $data['biaya_spp'];
		$content_data['tanggal'] = $tanggal;
		$content_data['tanggal_indo'] = tanggal_indo($tanggal);

		$data['content'] = $this->load->view('pages/laporan/index', $content_data, true);

		$this->load->view('template/app', $data);
	}

    public function laporanspp_data()
    {
        $param = $this->input->post();

        $result = $this->m_admin->laporanSpp($param)->result_array();

		// echo "<pre>";
		// print_r($result);
		// echo "</pre>";die;

		$bulan = [
			'01' => 'Januari',
			'02' => 'Februari',
			'03' => 'Maret',
			'04' => 'April',
			'05' => 'Mei',
			'06' => 'Juni',
			'07' => 'Juli',
			'08' => 'Agustus',
			'09' => 'September',
			'10' => 'Oktober',
			'11' => 'November',
			'12' => 'Desember'
		];

        if(empty($result)) {
            $return['data'] = '<p class="text-sm">*Tidak ada data laporan</p>';
        }else{
            $return['tanggal_awal'] = tanggal_indo(date('d-m-Y', strtotime($param['dateFr'])));
            $return['tanggal_akhir'] = tanggal_indo(date('d-m-Y', strtotime($param['dateTo'])));
            $return['data'] = '<table class="table table-striped">';
            $return['data'] .= '<thead class="bg-secondary"><tr><th>No</th><th>Kode Pembayaran</th><th>Nama Siswa</th><th>Nisn</th><th>Kelas</th><th>Periode SPP</th><th class="text-center">Tanggal Pembayaran</th><th class="text-end">Jumlah</th></tr></thead>';
            $no = 1;
			$total = 0;

            $return['data'] .= '<tbody>';
            foreach ($result as $key => $val){

                $return['data'] .= ' <tr><td>' . $no++ . '</td>';
                $return['data'] .= ' <td>' . $val['kode_pembayaran'] .'</td>';
                $return['data'] .= ' <td>' . $val['nama'] .'</td>';
                $return['data'] .= ' <td>' . $val['nisn'] .'</td>';
                $return['data'] .= ' <td>' . $val['kode_kelas'] .'</td>';
                $return['data'] .= ' <td>' . $bulan[$val['bulan']] .'</td>';
                $return['data'] .= ' <td class="text-center">' . $val['tgl_bayar'] .'</td>';
                $return['data'] .= ' <td class="text-end">' . number_format($val['jumlah'], 0, ',', '.') .'</td></tr>';
				$total += $val['jumlah'];
            }
			$return['data'] .= '<tr><td colspan="7" class="text-center">Total</td><td class="text-end">' . number_format($total, 0, ',', '.') . '</td></tr>';
            $return['data'] .= '</tbody>';
            $return['data'] .= '</table>';
        }

        echo json_encode($return);
    }

	

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('','refresh');
	}

}

/* End of file Admin.php */
/* Location: ./application/controllers/Admin.php */