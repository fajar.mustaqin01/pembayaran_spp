<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct(){
		parent::__construct();

		if(!empty($this->session->userdata('username'))){
			redirect('admin');
		}
	}

	public function index()
	{
		$this->form_validation->set_rules('username', 'Username', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');

		if ($this->form_validation->run() == FALSE) {

			$this->load->view('login/halaman_utama');

		} else {

			$this->_login();

		}
	}

	private function _login()
	{
		$username = $this->input->post('username', TRUE);
		$password = $this->input->post('password', TRUE);

		$data = $this->db->get_where('tbl_pengguna', ['xUser' => $username])->row_array();

		if ($data > 0) {
			if (password_verify($password, $data['xPass'])) {
				
				$array = array(
					'username' => $username,
					'nama' => $data['nama'],
					'id' => $data['id'],
					'image' => $data['image'],
					'level' => $data['level']
				);
				
				$this->session->set_userdata( $array );

				$this->session->set_flashdata('message', [
					'type' => 'success',
					'title' => 'Berhasil',
					'text' => 'Login berhasil'
				]);

				redirect('admin','refresh');

			} else {
				$this->session->set_flashdata('message', [
					'type' => 'error',
					'title' => 'Gagal',
					'text' => 'Password salah!'
				]);

				redirect('','refresh');
			}
			
		} else {
			$this->session->set_flashdata('message', [
				'type' => 'error',
				'title' => 'Gagal',
				'text' => 'Username tidak terdaftar!'
			]);
			
			redirect('','refresh');
		}
		
	}

}

/* End of file Login.php */
/* Location: ./application/controllers/Login.php */