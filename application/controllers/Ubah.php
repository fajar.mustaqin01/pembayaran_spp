<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ubah extends CI_Controller {

	function instansi()
	{
		if (! $this->session->userdata('username')) {
			redirect('','refresh');
		}
		$id_ = '10';
		$nama_instansi = $this->input->post('nama_instansi', TRUE);
		$id_instansi = $this->input->post('id_instansi', TRUE);
		$alamat = $this->input->post('alamat', TRUE);
		$email = $this->input->post('email', TRUE);
		$url = $this->input->post('url', TRUE);
		$tlp = $this->input->post('tlp', TRUE);
		$tp = $this->input->post('tp', TRUE);

		$data = [
			'nama' => $nama_instansi,
			'npsn' => $id_instansi,
			'alamat' => $alamat,
			'email' => $email,
			'site' => $url,
			'telepon' => $tlp,
			'th_pelajaran' => $tp,
		];

		$ubah = $this->m_ubah->instansi($data,$id_);

		if ($ubah) {
			$this->session->set_flashdata('success', 'Data instansi berhasil diubah');
			redirect('admin/instansi','refresh');
		} else {
			$this->session->set_flashdata('error', 'Data instansi gagal diubah');
			redirect('admin/instansi','refresh');
		}
	}

	function logo()
	{
		if (! $this->session->userdata('username')) {
			redirect('','refresh');
		}

		$upload = $this->m_ubah->logo();

		if ($upload == 'sukses') {
			$this->session->set_flashdata('success', 'Logo instansi berhasil diubah');
			redirect('admin/instansi','refresh');
		} else {
			$this->session->set_flashdata('error', $this->upload->display_errors());
			redirect('admin/instansi','refresh');
		}

	}

	function datauser()
	{

		$this->load->library('upload');
		$config['upload_path'] = './uploads/'; // Folder tujuan penyimpanan
        $config['allowed_types'] = 'jpg|jpeg|png|gif|pdf|docx'; // Jenis file yang diizinkan
        $config['max_size'] = 2048; // Ukuran maksimal file dalam KB (2 MB)

		$file_path = "";

		// Memuat konfigurasi ke library upload
		$this->upload->initialize($config);

		if (!$this->upload->do_upload('image')) {

			$error = array('error' => $this->upload->display_errors());

		} else {
			// Jika berhasil upload, ambil informasi file
			$data = $this->upload->data();
	
			// Mendapatkan path lengkap dari file yang diunggah
			$file_path = $data['file_name'];

		}


	 	$username = $this->input->post('xUser');
	    $nama = $this->input->post('nama');
	    $email = $this->input->post('email');
	    $password = $this->input->post('xPass');
	    $id = $this->session->userdata('id');

	    $data = [
	    	'xUser' => $username,
	    	'nama' => $nama,
	    	'email' => $email
	    ];

		if($password != ""){
			$data['xPass'] = password_hash($password, PASSWORD_DEFAULT);
		}

		if($file_path != ""){
			$data['image'] = $file_path;
		}

	   $ubah = $this->m_ubah->datauser($data,$id);

	   if ($ubah) {
			$this->session->set_userdata('username', $username);
			$this->session->set_userdata('nama', $nama);
			if($file_path != ""){
				$this->session->set_userdata('image', $file_path);
			}
			

			$this->session->set_flashdata('message', [
				'type' => 'success',
				'title' => 'Berhasil',
				'text' => 'update profile sukses'
			]);
		} else {
			$this->session->set_flashdata('message', [
				'type' => 'error',
				'title' => 'Gagal',
				'text' => 'update profile gagal'
			]);
		}
		redirect('admin/profile','refresh');
	}

	function kelasSiswa()
	{
		if (! $this->session->userdata('username')) {
			redirect('','refresh');
		}

		//error_reporting(0);
		$kode_kelas = $this->input->post('kodeKelas');
		$id = $this->input->post('check');

		$data = ['kode_kelas' => $kode_kelas];

		if ($id) {
			$in = 0;
			foreach ($id as $key) {
				$nisn = $key;
				$in++;

				$this->db->where('nisn', $nisn);
				$this->db->update('tbl_siswa', $data);

			}

			$this->session->set_flashdata('success', 'Kelas siswa berhasil diubah');
			redirect('admin/siswa','refresh');
		} else {
			$this->session->set_flashdata('error', 'Siswa belum dipilih');
			redirect('admin/siswa','refresh');
		}


	}

	function datakelas()
	{
	 	$id = $this->input->post('id_kelas');
	    $kode_kelas = $this->input->post('kode_kelas');
	    $nama_kelas = $this->input->post('nama_kelas');

	    $data = [
	    	'kode_kelas' => $kode_kelas,
	    	'nama_kelas' => $nama_kelas
	    ];

	   $ubah = $this->m_ubah->datakelas($data,$id);

	   if ($ubah) {
			$this->session->set_flashdata('message', [
				'type' => 'success',
				'title' => 'Berhasil',
				'text' => 'Data siswa berhasil diubah.'
			]);
		} else {
			$this->session->set_flashdata('message', [
				'type' => 'error',
				'title' => 'Gagal',
				'text' => 'Data siswa gagal diubah.'
			]);
		}
	
		redirect('admin/kelas', 'refresh');
	}

	function datasiswa()
	{
		$id = $this->input->post('id');
		$data = [
		    'nama' => $this->input->post('nama'),
		    'nisn' => $this->input->post('nisn'),
		    'email' => $this->input->post('email'),
		    'jk' => $this->input->post('jk'),
		    'tempat_lahir' => $this->input->post('tempat_lahir'),
		    'tg_lahir' => $this->input->post('tg_lahir'),
		    'kelas_id' => $this->input->post('id_kelas')
		];

		$ubah = $this->m_ubah->datasiswa($data,$id);

		if ($ubah) {
			$this->session->set_flashdata('message', [
				'type' => 'success',
				'title' => 'Berhasil',
				'text' => 'Data siswa berhasil diubah.'
			]);
		} else {
			$this->session->set_flashdata('message', [
				'type' => 'error',
				'title' => 'Gagal',
				'text' => 'Data siswa gagal diubah.'
			]);
		}
		
		redirect('admin/siswa', 'refresh');
	}

	function datauser_admin()
	{
		$id = $this->input->post('id');
		$data = [
		    'nama' => $this->input->post('nama'),
		    'email' => $this->input->post('email')
		];

		$ubah = $this->m_ubah->datauser($data,$id);

		if ($ubah) {
			$this->session->set_flashdata('message', [
				'type' => 'success',
				'title' => 'Berhasil',
				'text' => 'Data user berhasil diubah.'
			]);
		} else {
			$this->session->set_flashdata('message', [
				'type' => 'error',
				'title' => 'Gagal',
				'text' => 'Data user gagal diubah.'
			]);
		}
		redirect('admin/users','refresh');
	}

	function biaya()
	{
		$id = $this->input->post('id');

		$data = [
	    'jenis' => $this->input->post('jenis'),
	    'kode' => $this->input->post('kode'),
	    'deskripsi' => $this->input->post('deskripsi'),
	    'jumlah' => $this->input->post('jumlah'),
		];

		$ubah = $this->m_ubah->biaya($data,$id);

		 if ($ubah) {
			$this->session->set_flashdata('success', 'Data biaya berhasil diubah');
			redirect('admin/biaya','refresh');
		} else {
			$this->session->set_flashdata('error', 'Data biaya gagal diubah');
		}
		redirect('admin/biaya','refresh');
	}

	
}

/* End of file Ubah.php */
/* Location: ./application/controllers/Ubah.php



		echo "<pre>";
		print_r($_POST);
		echo "</pre>";


 */

