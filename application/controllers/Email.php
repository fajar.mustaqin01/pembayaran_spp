<?php

defined('BASEPATH') OR exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class Email extends CI_Controller {

    public function __construct(){
		parent::__construct();

		if (! $this->session->userdata('username')) {
			redirect('','refresh');
		}
	}
    
    public function sendEmail() {
        $this->load->library('email');

        $get = $this->input->get('id');

        $result = $this->db->query("SELECT * FROM tbl_siswa WHERE id_siswa = '$get'")->result_array()[0];

        $biaya = $this->db->query("SELECT jumlah FROM tbl_biaya")->result_array()[0];

        $config = array(
            'protocol' => 'smtp',
            'smtp_host' => 'smtp.gmail.com',
            'smtp_port' => 465,
            'smtp_user' => 'pusparini.tk01@gmail.com',
            'smtp_pass' => 'uztj pkre yqtm haei',
            'smtp_crypto' => 'ssl', // atau coba kosongkan jika masalah tetap ada
            'mailtype'  => 'html',
            'charset'   => 'iso-8859-1',
            'wordwrap'  => TRUE
        );
        $this->email->initialize($config);

        $bulan = date('F'); 
        $tahun = date('Y'); 

        $namaOrangTua = "Bapak/Ibu ". $result['nama']; // Ambil dari database jika perlu
        $namaBulan = $bulan . " " .$tahun ;
        $nominal = $biaya['jumlah'];
        $tanggalJatuhTempo = "15 $bulan $tahun";
        $namaSekolah = "TK Pusparini";
        $kontakSekolah = "0812-3456-7890";
        $namaPengirim = "Admin Sekolah";
        $posisiPengirim = "Tata Usaha";

        // Isi email
        $message = "
            <p>Yth. <strong>$namaOrangTua</strong>,</p>
            <p>Kami dari <strong>$namaSekolah</strong> ingin menginformasikan bahwa pembayaran SPP untuk bulan <strong>$namaBulan</strong> sebesar <strong>Rp $nominal</strong> jatuh tempo pada <strong>$tanggalJatuhTempo</strong>.</p>
            <p>Mohon segera melakukan pembayaran melalui rekening berikut:</p>
            <ul>
                <li><strong>Bank:</strong> BCA</li>
                <li><strong>Nomor Rekening:</strong> 123-456-789</li>
                <li><strong>Atas Nama:</strong> TK Pusparini</li>
            </ul>
            <p>Jika sudah melakukan pembayaran, harap mengirimkan bukti pembayaran ke bagian tata usaha atau melalui email ini.</p>
            <p>Terima kasih atas perhatian dan kerjasamanya.</p>
            <p>Hormat kami,</p>
            <p><strong>$namaPengirim</strong><br>
            <em>$posisiPengirim</em><br>
            $kontakSekolah</p>
        ";

        $this->email->set_newline("\r\n");
        $this->email->from('pusparini.tk01@gmail.com', 'TK Pusparini');
        $this->email->to($result['email']);
        $this->email->subject('Pemberitahuan Pembayaran SPP');
        $this->email->message($message);

        if ($this->email->send()) {
            $this->session->set_flashdata('message', [
                'type' => 'success',
                'title' => 'berhasil',
                'text' => 'Berhasil mengirim email.'
            ]);
        } else {
            $this->session->set_flashdata('message', [
                'type' => 'error',
                'title' => 'Gagal',
                'text' => 'gagal mengirim email'
            ]);
        }

        redirect('admin/pengingat', 'refresh');
    }
}
