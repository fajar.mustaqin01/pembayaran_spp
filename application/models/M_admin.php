<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_admin extends CI_Model {

	function data_instansi()
	{
		return $this->db->get('tbl_instansi')->row_array();
	}

	function data_kelas()
	{
		return $this->db->get('tbl_kelas');
	}

	function data_user($id){
		
		$data = $this->db->where('id', $id)
						->get('tbl_pengguna')
						->result_array();

		return $data;
	}

	function data_users(){
		return $this->db->get('tbl_pengguna');
	}

	function jumlah_siswa()
	{
		return $this->db->get('tbl_siswa')->num_rows();
	}

	function data_siswa()
	{
		$siswa = $this->db->select('*, tbl_siswa.id_siswa as ids')
				->order_by('nama', 'asc')
				->join('tbl_kelas', 'tbl_kelas.id_kelas = tbl_siswa.kelas_id', 'left')
				->get('tbl_siswa');

		return $siswa;
	}

	function laporan_keuangan()
	{
		$bulan = date('m');
		$siswa = $this->db->query("SELECT s.nisn, s.nama, k.nama_kelas, s.id_siswa, COALESCE((SELECT SUM(p.jumlah) AS total FROM tbl_pembayaran p WHERE p.nisn = s.nisn AND p.bulan = $bulan AND p.th_pelajaran = '2024/2025' AND p.jenis = 'SPP'), 0) AS total_pembayaran FROM tbl_siswa s INNER JOIN tbl_kelas k ON k.id_kelas = s.kelas_id");

		return $siswa;
	}

	function biaya1()
	{
		return $this->db->get('tbl_biaya');
	}

	function biaya_spp(){
		$this->db->select('jumlah');
		$this->db->where('jenis', 'SPP');
		$return = $this->db->get('tbl_biaya');
		return $return;
	}

	function simpan_spp($data)
	{
		$simpan = $this->db->insert('tbl_pembayaran', $data);
		if ($simpan) {
			return TRUE;
		} else {
			return FALSE;
		}
		
	}
	
	function jenis_biaya()
	{
		$this->db->select('jenis');
		$this->db->distinct();
		$query = $this->db->get('tbl_biaya');
		return $query->result();
	}
	

	function jenis_pembayaran()
	{	
		$this->db->select('jenis, COUNT(*) as total');
		$this->db->group_by('jenis');
		$query = $this->db->get('tbl_biaya');
		return $query->num_rows();
	}

	function jum_uang()
	{
		$j = $this->db->get('tbl_pembayaran')->result_array();
		$jl = array();
		foreach ($j as $key) {
			array_push($jl, $key['jumlah']);
		}
		$jlm = array_sum($jl);
		return $jlm;
	}

    public function laporanSpp($params)
    {

		$result = $this->db->query("SELECT p.*, s.nama, k.kode_kelas  FROM tbl_pembayaran p INNER JOIN tbl_siswa s ON p.nisn = s.nisn INNER JOIN tbl_kelas k ON k.id_kelas = s.kelas_id WHERE tgl_bayar BETWEEN '{$params['dateFr']}' AND '{$params['dateTo']}'");

        return $result;
    }


}

/* End of file M_admin.php */
/* Location: ./application/models/M_admin.php */