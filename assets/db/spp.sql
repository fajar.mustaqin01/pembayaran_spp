-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               8.0.30 - MySQL Community Server - GPL
-- Server OS:                    Win64
-- HeidiSQL Version:             12.6.0.6765
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for spp
CREATE DATABASE IF NOT EXISTS `spp` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `spp`;

-- Dumping structure for table spp.tbl_biaya
CREATE TABLE IF NOT EXISTS `tbl_biaya` (
  `id_` int NOT NULL AUTO_INCREMENT,
  `jenis` varchar(50) NOT NULL,
  `kode` varchar(15) NOT NULL,
  `deskripsi` text NOT NULL,
  `jumlah` varchar(15) NOT NULL,
  `status` enum('1','0') NOT NULL,
  PRIMARY KEY (`id_`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table spp.tbl_biaya: ~2 rows (approximately)
INSERT INTO `tbl_biaya` (`id_`, `jenis`, `kode`, `deskripsi`, `jumlah`, `status`) VALUES
	(1, 'SPP', '#1', 'SPP bulanan', '40000', '1'),
	(14, 'Seragam', '#2', 'Biaya Seragam', '150000', '1');

-- Dumping structure for table spp.tbl_instansi
CREATE TABLE IF NOT EXISTS `tbl_instansi` (
  `id_` int NOT NULL AUTO_INCREMENT,
  `nama` varchar(30) NOT NULL,
  `npsn` varchar(10) NOT NULL,
  `alamat` text NOT NULL,
  `email` varchar(30) NOT NULL,
  `site` varchar(30) NOT NULL,
  `telepon` varchar(20) NOT NULL,
  `th_pelajaran` varchar(15) NOT NULL,
  `logo` varchar(20) NOT NULL,
  PRIMARY KEY (`id_`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table spp.tbl_instansi: ~1 rows (approximately)
INSERT INTO `tbl_instansi` (`id_`, `nama`, `npsn`, `alamat`, `email`, `site`, `telepon`, `th_pelajaran`, `logo`) VALUES
	(10, 'TK Pusparini', '30300700', 'Jl. A Yani Km 96 Jorong', 'tk_pusparini@gmail.com', 'https://tkpusparini', '082153240501', '2024/2025', 'logo_instansi.jpg');

-- Dumping structure for table spp.tbl_kelas
CREATE TABLE IF NOT EXISTS `tbl_kelas` (
  `id_` int NOT NULL AUTO_INCREMENT,
  `kode_kelas` varchar(10) NOT NULL,
  `nama_kelas` varchar(15) NOT NULL,
  PRIMARY KEY (`id_`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table spp.tbl_kelas: ~1 rows (approximately)
INSERT INTO `tbl_kelas` (`id_`, `kode_kelas`, `nama_kelas`) VALUES
	(16, 'TK-A', 'TK-A');

-- Dumping structure for table spp.tbl_pembayaran
CREATE TABLE IF NOT EXISTS `tbl_pembayaran` (
  `id_` int NOT NULL AUTO_INCREMENT,
  `nisn` varchar(15) NOT NULL,
  `jenis` varchar(30) NOT NULL,
  `tgl_bayar` date NOT NULL,
  `bulan` varchar(2) NOT NULL,
  `th_pelajaran` varchar(15) NOT NULL,
  `petugas` varchar(30) NOT NULL,
  `jumlah` varchar(12) NOT NULL,
  `seharusnya` varchar(12) NOT NULL,
  `selisih` varchar(12) NOT NULL,
  `kode_pembayaran` varchar(15) NOT NULL,
  `deskripsi` text NOT NULL,
  PRIMARY KEY (`id_`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table spp.tbl_pembayaran: ~3 rows (approximately)
INSERT INTO `tbl_pembayaran` (`id_`, `nisn`, `jenis`, `tgl_bayar`, `bulan`, `th_pelajaran`, `petugas`, `jumlah`, `seharusnya`, `selisih`, `kode_pembayaran`, `deskripsi`) VALUES
	(34, '2384234', 'SPP', '2024-06-26', '06', '2024/2025', 'Administrator', '40000', '40000', '0', 'B-00001', 'SPP bulanan'),
	(35, '2384234', 'SPP', '2024-09-13', '10', '2024/2025', 'Administrator', '40000', '40000', '0', 'B-00002', 'SPP bulanan'),
	(36, '2884723498', 'SPP', '2024-10-18', '09', '2024/2025', 'Administrator', '40000', '40000', '0', 'B-00003', 'SPP bulanan');

-- Dumping structure for table spp.tbl_pengguna
CREATE TABLE IF NOT EXISTS `tbl_pengguna` (
  `id` int NOT NULL AUTO_INCREMENT,
  `xUser` varchar(50) NOT NULL,
  `xPass` varchar(225) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `image` varchar(250) NOT NULL,
  `level` enum('1','2','3') CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '2',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table spp.tbl_pengguna: ~1 rows (approximately)
INSERT INTO `tbl_pengguna` (`id`, `xUser`, `xPass`, `nama`, `email`, `image`, `level`) VALUES
	(1, 'admin', '$2y$10$d6WBeK6IfJC/nmmc39G5nO6QnVxRKgk66JYiAV1rGUdygnsEPJqGy', 'Administrator', 'admin@gmail.com', '', '1');

-- Dumping structure for table spp.tbl_siswa
CREATE TABLE IF NOT EXISTS `tbl_siswa` (
  `id_` int NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `nisn` varchar(10) NOT NULL,
  `jk` enum('1','2') NOT NULL,
  `tempat_lahir` varchar(30) NOT NULL,
  `tg_lahir` varchar(20) NOT NULL,
  `kode_kelas` varchar(15) NOT NULL,
  `email` varchar(50) NOT NULL,
  PRIMARY KEY (`id_`)
) ENGINE=InnoDB AUTO_INCREMENT=105 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table spp.tbl_siswa: ~2 rows (approximately)
INSERT INTO `tbl_siswa` (`id_`, `nama`, `nisn`, `jk`, `tempat_lahir`, `tg_lahir`, `kode_kelas`, `email`) VALUES
	(103, 'zidan', '2384234', '1', 'Bandung', '2020-01-07', 'TK-A', ''),
	(104, 'sinta', '2884723498', '2', 'demak', '2019-01-29', 'TK-A', '');

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
