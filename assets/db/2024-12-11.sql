-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               8.0.30 - MySQL Community Server - GPL
-- Server OS:                    Win64
-- HeidiSQL Version:             12.6.0.6765
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for spp
CREATE DATABASE IF NOT EXISTS `spp` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `spp`;

-- Dumping structure for table spp.tbl_biaya
CREATE TABLE IF NOT EXISTS `tbl_biaya` (
  `id_biaya` int NOT NULL AUTO_INCREMENT,
  `jenis` varchar(50) NOT NULL,
  `kode` varchar(15) NOT NULL,
  `deskripsi` text NOT NULL,
  `jumlah` varchar(15) NOT NULL,
  `status` enum('1','0') NOT NULL,
  PRIMARY KEY (`id_biaya`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table spp.tbl_biaya: ~1 rows (approximately)
INSERT INTO `tbl_biaya` (`id_biaya`, `jenis`, `kode`, `deskripsi`, `jumlah`, `status`) VALUES
	(1, 'SPP', '#1', 'SPP bulanan', '40000', '1');

-- Dumping structure for table spp.tbl_instansi
CREATE TABLE IF NOT EXISTS `tbl_instansi` (
  `id_` int NOT NULL AUTO_INCREMENT,
  `nama` varchar(30) NOT NULL,
  `npsn` varchar(10) NOT NULL,
  `alamat` text NOT NULL,
  `email` varchar(30) NOT NULL,
  `site` varchar(30) NOT NULL,
  `telepon` varchar(20) NOT NULL,
  `th_pelajaran` varchar(15) NOT NULL,
  `logo` varchar(20) NOT NULL,
  PRIMARY KEY (`id_`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table spp.tbl_instansi: ~1 rows (approximately)
INSERT INTO `tbl_instansi` (`id_`, `nama`, `npsn`, `alamat`, `email`, `site`, `telepon`, `th_pelajaran`, `logo`) VALUES
	(10, 'TK Pusparini', '30300700', 'Jl. A Yani Km 96 Jorong', 'tk_pusparini@gmail.com', 'https://tkpusparini', '082153240501', '2024/2025', 'logo_instansi.jpg');

-- Dumping structure for table spp.tbl_kelas
CREATE TABLE IF NOT EXISTS `tbl_kelas` (
  `id_kelas` int NOT NULL AUTO_INCREMENT,
  `kode_kelas` varchar(10) NOT NULL,
  `nama_kelas` varchar(15) NOT NULL,
  PRIMARY KEY (`id_kelas`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table spp.tbl_kelas: ~1 rows (approximately)
INSERT INTO `tbl_kelas` (`id_kelas`, `kode_kelas`, `nama_kelas`) VALUES
	(16, 'TK-A', 'kelas belajar');

-- Dumping structure for table spp.tbl_pembayaran
CREATE TABLE IF NOT EXISTS `tbl_pembayaran` (
  `id_pembayaran` int NOT NULL AUTO_INCREMENT,
  `nisn` varchar(15) NOT NULL,
  `kode_pembayaran` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `jenis` varchar(30) NOT NULL,
  `tgl_bayar` date NOT NULL,
  `bulan` varchar(2) NOT NULL,
  `th_pelajaran` varchar(15) NOT NULL,
  `jumlah` varchar(12) NOT NULL,
  `seharusnya` varchar(12) NOT NULL,
  `selisih` varchar(12) NOT NULL,
  `deskripsi` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `user_id` int NOT NULL DEFAULT '0',
  `biaya_id` int NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_pembayaran`) USING BTREE,
  KEY `FK_tbl_pembayaran_tbl_pengguna` (`user_id`),
  KEY `FK_tbl_pembayaran_tbl_biaya` (`biaya_id`),
  CONSTRAINT `FK_tbl_pembayaran_tbl_biaya` FOREIGN KEY (`biaya_id`) REFERENCES `tbl_biaya` (`id_biaya`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `FK_tbl_pembayaran_tbl_pengguna` FOREIGN KEY (`user_id`) REFERENCES `tbl_pengguna` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table spp.tbl_pembayaran: ~0 rows (approximately)

-- Dumping structure for table spp.tbl_pengguna
CREATE TABLE IF NOT EXISTS `tbl_pengguna` (
  `id` int NOT NULL AUTO_INCREMENT,
  `xUser` varchar(50) NOT NULL,
  `xPass` varchar(225) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `image` varchar(250) NOT NULL,
  `level` enum('1','2','3') CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '2',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table spp.tbl_pengguna: ~1 rows (approximately)
INSERT INTO `tbl_pengguna` (`id`, `xUser`, `xPass`, `nama`, `email`, `image`, `level`) VALUES
	(1, 'admin', '$2y$10$kIoLzttpGYr47QnO5sPBYO/uBWopKoY9nbo87xTh9E2cWCleIvSW.', 'Administrator', 'admin@gmail.com', '', '1');

-- Dumping structure for table spp.tbl_siswa
CREATE TABLE IF NOT EXISTS `tbl_siswa` (
  `id_siswa` int NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `nisn` varchar(10) NOT NULL,
  `jk` enum('1','2') NOT NULL,
  `tempat_lahir` varchar(30) NOT NULL,
  `tg_lahir` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `kelas_id` int NOT NULL DEFAULT (0),
  PRIMARY KEY (`id_siswa`) USING BTREE,
  KEY `FK_tbl_siswa_tbl_kelas` (`kelas_id`),
  CONSTRAINT `FK_tbl_siswa_tbl_kelas` FOREIGN KEY (`kelas_id`) REFERENCES `tbl_kelas` (`id_kelas`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE=InnoDB AUTO_INCREMENT=117 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Dumping data for table spp.tbl_siswa: ~0 rows (approximately)

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
