<?php return array(
    'root' => array(
        'name' => '__root__',
        'pretty_version' => 'dev-main',
        'version' => 'dev-main',
        'reference' => 'd3bde9ec45cebc82b3817770233cb51c040894ff',
        'type' => 'library',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'dev' => true,
    ),
    'versions' => array(
        '__root__' => array(
            'pretty_version' => 'dev-main',
            'version' => 'dev-main',
            'reference' => 'd3bde9ec45cebc82b3817770233cb51c040894ff',
            'type' => 'library',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'beberlei/assert' => array(
            'pretty_version' => 'v3.3.2',
            'version' => '3.3.2.0',
            'reference' => 'cb70015c04be1baee6f5f5c953703347c0ac1655',
            'type' => 'library',
            'install_path' => __DIR__ . '/../beberlei/assert',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'clue/stream-filter' => array(
            'pretty_version' => 'v1.7.0',
            'version' => '1.7.0.0',
            'reference' => '049509fef80032cb3f051595029ab75b49a3c2f7',
            'type' => 'library',
            'install_path' => __DIR__ . '/../clue/stream-filter',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'illuminate/collections' => array(
            'pretty_version' => 'v8.83.27',
            'version' => '8.83.27.0',
            'reference' => '705a4e1ef93cd492c45b9b3e7911cccc990a07f4',
            'type' => 'library',
            'install_path' => __DIR__ . '/../illuminate/collections',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'illuminate/contracts' => array(
            'pretty_version' => 'v8.83.27',
            'version' => '8.83.27.0',
            'reference' => '5e0fd287a1b22a6b346a9f7cd484d8cf0234585d',
            'type' => 'library',
            'install_path' => __DIR__ . '/../illuminate/contracts',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'illuminate/macroable' => array(
            'pretty_version' => 'v8.83.27',
            'version' => '8.83.27.0',
            'reference' => 'aed81891a6e046fdee72edd497f822190f61c162',
            'type' => 'library',
            'install_path' => __DIR__ . '/../illuminate/macroable',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'mailersend/mailersend' => array(
            'pretty_version' => 'v0.25.0',
            'version' => '0.25.0.0',
            'reference' => '7fba98fb307f7e69df31f57364b4855ab45d661b',
            'type' => 'library',
            'install_path' => __DIR__ . '/../mailersend/mailersend',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'nyholm/psr7' => array(
            'pretty_version' => '1.8.1',
            'version' => '1.8.1.0',
            'reference' => 'aa5fc277a4f5508013d571341ade0c3886d4d00e',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nyholm/psr7',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'php-http/async-client-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '*',
            ),
        ),
        'php-http/client-common' => array(
            'pretty_version' => '2.7.1',
            'version' => '2.7.1.0',
            'reference' => '1e19c059b0e4d5f717bf5d524d616165aeab0612',
            'type' => 'library',
            'install_path' => __DIR__ . '/../php-http/client-common',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'php-http/client-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '*',
            ),
        ),
        'php-http/discovery' => array(
            'pretty_version' => '1.19.4',
            'version' => '1.19.4.0',
            'reference' => '0700efda8d7526335132360167315fdab3aeb599',
            'type' => 'composer-plugin',
            'install_path' => __DIR__ . '/../php-http/discovery',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'php-http/httplug' => array(
            'pretty_version' => '2.4.0',
            'version' => '2.4.0.0',
            'reference' => '625ad742c360c8ac580fcc647a1541d29e257f67',
            'type' => 'library',
            'install_path' => __DIR__ . '/../php-http/httplug',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'php-http/message' => array(
            'pretty_version' => '1.16.1',
            'version' => '1.16.1.0',
            'reference' => '5997f3289332c699fa2545c427826272498a2088',
            'type' => 'library',
            'install_path' => __DIR__ . '/../php-http/message',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'php-http/message-factory-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0',
            ),
        ),
        'php-http/promise' => array(
            'pretty_version' => '1.3.1',
            'version' => '1.3.1.0',
            'reference' => 'fc85b1fba37c169a69a07ef0d5a8075770cc1f83',
            'type' => 'library',
            'install_path' => __DIR__ . '/../php-http/promise',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'phpmailer/phpmailer' => array(
            'pretty_version' => 'v6.9.1',
            'version' => '6.9.1.0',
            'reference' => '039de174cd9c17a8389754d3b877a2ed22743e18',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpmailer/phpmailer',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'psr/container' => array(
            'pretty_version' => '1.1.2',
            'version' => '1.1.2.0',
            'reference' => '513e0666f7216c7459170d56df27dfcefe1689ea',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/container',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'psr/http-client' => array(
            'pretty_version' => '1.0.3',
            'version' => '1.0.3.0',
            'reference' => 'bb5906edc1c324c9a05aa0873d40117941e5fa90',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/http-client',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'psr/http-client-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '*',
                1 => '1.0',
            ),
        ),
        'psr/http-factory' => array(
            'pretty_version' => '1.1.0',
            'version' => '1.1.0.0',
            'reference' => '2b4765fddfe3b508ac62f829e852b1501d3f6e8a',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/http-factory',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'psr/http-factory-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '*',
                1 => '1.0',
            ),
        ),
        'psr/http-message' => array(
            'pretty_version' => '2.0',
            'version' => '2.0.0.0',
            'reference' => '402d35bcb92c70c026d1a6a9883f06b2ead23d71',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/http-message',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'psr/http-message-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '*',
                1 => '1.0',
            ),
        ),
        'psr/log' => array(
            'pretty_version' => '1.1.4',
            'version' => '1.1.4.0',
            'reference' => 'd49695b909c3b7628b6289db5479a1c204601f11',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/log',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'psr/simple-cache' => array(
            'pretty_version' => '1.0.1',
            'version' => '1.0.1.0',
            'reference' => '408d5eafb83c57f6365a3ca330ff23aa4a5fa39b',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/simple-cache',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/deprecation-contracts' => array(
            'pretty_version' => 'v2.5.3',
            'version' => '2.5.3.0',
            'reference' => '80d075412b557d41002320b96a096ca65aa2c98d',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/deprecation-contracts',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/http-client' => array(
            'pretty_version' => 'v5.4.42',
            'version' => '5.4.42.0',
            'reference' => 'ed17728617f53903ac684aa4f7a1739f121798d3',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/http-client',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/http-client-contracts' => array(
            'pretty_version' => 'v2.5.3',
            'version' => '2.5.3.0',
            'reference' => 'e5cc97c2b4a4db0ba26bebc154f1426e3fd1d2f1',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/http-client-contracts',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/http-client-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '2.4',
            ),
        ),
        'symfony/options-resolver' => array(
            'pretty_version' => 'v5.4.40',
            'version' => '5.4.40.0',
            'reference' => 'bd1afbde6613a8d6b956115e0e14b196191fd0c4',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/options-resolver',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php73' => array(
            'pretty_version' => 'v1.30.0',
            'version' => '1.30.0.0',
            'reference' => 'ec444d3f3f6505bb28d11afa41e75faadebc10a1',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php73',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php80' => array(
            'pretty_version' => 'v1.30.0',
            'version' => '1.30.0.0',
            'reference' => '77fa7995ac1b21ab60769b7323d600a991a90433',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php80',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
        'symfony/service-contracts' => array(
            'pretty_version' => 'v2.5.3',
            'version' => '2.5.3.0',
            'reference' => 'a2329596ddc8fd568900e3fc76cba42489ecc7f3',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/service-contracts',
            'aliases' => array(),
            'dev_requirement' => false,
        ),
    ),
);
